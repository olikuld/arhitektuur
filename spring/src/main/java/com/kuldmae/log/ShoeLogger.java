package com.kuldmae.log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;

public class ShoeLogger {

    private Logger logger;

    public ShoeLogger() {
        logger = Logger.getLogger("shoeLogger");
        FileHandler fh;

        try {
            fh = new FileHandler("log.txt");
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            Formatter formatter = new LogFormatter();
            fh.setFormatter(formatter);
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    public void log(String message) {
        logger.info(message);
    }
}
