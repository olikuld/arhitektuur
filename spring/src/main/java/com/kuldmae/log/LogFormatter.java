package com.kuldmae.log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

class LogFormatter extends Formatter {

    public String format(LogRecord rec) {
        StringBuffer buf = new StringBuffer(1000);
        buf.append("<tr>");
        buf.append("<td>");
        buf.append(rec.getLevel());
        buf.append("</td>");
        buf.append("<td>");
        buf.append(calcDate(rec.getMillis()));
        buf.append("</td>");
        buf.append("<td>");
        buf.append(formatMessage(rec));
        buf.append("</td>");
        buf.append("</tr>");
        return buf.toString();
    }

    private String calcDate(long millisecs) {
        SimpleDateFormat date_format = new SimpleDateFormat("MMM dd, yyyy HH:mm");
        Date resultdate = new Date(millisecs);
        return date_format.format(resultdate);
    }

    public String getHead(Handler h) {
        return "<h3>" + (new Date()) + "</h3>"
                + "<table class=\"u-full-width\">"
                + "<thead>"
                + "<tr>"
                + "<th>Level</th>"
                + "<th>Time</th>"
                + "<th>Message</th>"
                + "</tr>"
                + "<thead>"
                + "<tbody>";
    }

    public String getTail(Handler h) {
        return "</tbody></table><a href=\"/shoe/s\" class=\"button\">Back</a>";
    }
}