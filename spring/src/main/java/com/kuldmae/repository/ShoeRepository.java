package com.kuldmae.repository;

import com.kuldmae.model.Shoe;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ShoeRepository extends CrudRepository<Shoe, Integer> {

    public List<Shoe> findAllByOrderByIdAsc();

}