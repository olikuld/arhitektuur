package com.kuldmae;

import com.kuldmae.log.ShoeLogger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		ShoeLogger logger = new ShoeLogger();
		logger.log("I have been created!");
	}
}
