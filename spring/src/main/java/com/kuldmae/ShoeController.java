package com.kuldmae;

import com.kuldmae.log.ShoeLogger;
import com.kuldmae.model.Shoe;
import com.kuldmae.repository.ShoeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@Controller
@EnableAutoConfiguration
@RequestMapping("shoe")
public class ShoeController {

    @Autowired
    private ShoeRepository shoeRepository;

    private ShoeLogger logger = new ShoeLogger();

    @RequestMapping(value = "s", method = RequestMethod.GET)
    public String shoe(@RequestParam(value = "id", required = false) Integer id, Model model) {
        if (id != null) {
            logger.log("ShoeController.findById(): " + id);
            model.addAttribute("shoe", shoeRepository.findOne(id));
            return "shoe";
        }

        logger.log("ShoeController.findAll()");
        model.addAttribute("shoes", shoeRepository.findAllByOrderByIdAsc());
        return "shoes";
    }

    @RequestMapping(value = "s", method = RequestMethod.POST)
    public String saveShoe(
            @RequestParam(value = "action") String action,
            @Valid @ModelAttribute("shoe") Shoe shoe,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            logger.log("ShoeController.save() failed: " + shoe.getId());
            return "shoe";
        }

        if (action.equals("save")) {
            logger.log("ShoeController.save(): " + shoe.getId());
            shoeRepository.save(shoe);
        }

        return "redirect:/shoe/s?id=" + shoe.getId();
    }

    @RequestMapping(value = "shoeservice", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Shoe shoeService(@RequestParam(value = "id") Integer id) {
        logger.log("ShoeService.findOne():" + id);
        return shoeRepository.findOne(id);
    }

     @RequestMapping("log")
     public String shoeLog(Model model) {
         String fileName = "log.txt";
         String result = "";

         try {
             result = Files.lines(Paths.get(fileName)).collect(Collectors.joining());
         } catch (IOException e) {
             e.printStackTrace();
         }

         model.addAttribute("log", result);

         return "log";
     }

}