package com.kuldmae.model;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Shoe {

    @Id
    @SequenceGenerator(name="shoe_idshoe_seq", sequenceName="shoe_idshoe_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="shoe_idshoe_seq")
    private Integer id;

    @NotNull(message = "Shoe must have a name!")
    @Column(length = 20)
    @Length(min = 1, max = 20, message = "Shoe name must be between 1 and 20 characters long!")
    private String name;

    @NotNull(message = "Shoe must have a price!")
    @Range(min = 0, max = 99999, message = "Price must be between 0 and 99999!")
    private Integer price;

    @Column(columnDefinition="TEXT")
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
