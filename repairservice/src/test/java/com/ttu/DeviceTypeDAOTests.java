package com.ttu;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import com.ttu.dao.DeviceTypeDAO;
import com.ttu.model.repairService.DeviceType;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RepairServiceApplication.class)
@WebAppConfiguration
public class DeviceTypeDAOTests {

    @Autowired
    DeviceTypeDAO dao;

    @org.junit.Test
    public void testInsertAndDelete() throws Exception {
        // Create a new device type
        DeviceType deviceType = new DeviceType(1, "deviceTypeName");
        assertEquals(0, deviceType.getDeviceType());

        // Check if ID field was populated
        dao.insert(deviceType);
        assertNotNull(deviceType.getDeviceType());

        // Check if insert was successful
        DeviceType foundDeviceType = dao.findOne(deviceType.getDeviceType());
        assertEquals(deviceType, foundDeviceType);

        // Delete the object
        dao.delete(deviceType);
        foundDeviceType = dao.findOne(deviceType.getDeviceType());

        // Make sure it does not exist
        assertNull(foundDeviceType);
    }

    @org.junit.Test
    public void testFindAll() throws Exception {
        List<DeviceType> devicesTypes = dao.findAll();
        assertEquals(15, devicesTypes.size());
    }
}