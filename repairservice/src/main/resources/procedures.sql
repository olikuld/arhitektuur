CREATE OR REPLACE FUNCTION check_invoice_row_type() RETURNS TRIGGER AS $check_invoice_row_type$
BEGIN
  IF NEW.service_action IS NOT NULL AND NEW.invoice_row_type != 1 THEN
    RAISE EXCEPTION 'Teenuse rea tuup on 2!';
  ELSIF NEW.service_part IS NOT NULL AND NEW.invoice_row_type != 2 THEN
    RAISE EXCEPTION 'Osa rea tuup on 2!';
  END IF;
END;
$check_invoice_row_type$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS check_invoice_row_type_trigger ON invoice_row;

CREATE TRIGGER check_invoice_row_type_trigger
BEFORE INSERT OR UPDATE ON invoice_row
FOR EACH ROW
EXECUTE PROCEDURE check_invoice_row_type();