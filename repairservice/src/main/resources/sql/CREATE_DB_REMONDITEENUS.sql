-- SEQUENCES --
CREATE SEQUENCE service_request_id;
CREATE SEQUENCE device_type_id;
CREATE SEQUENCE service_order_id;
CREATE SEQUENCE service_note_id;
CREATE SEQUENCE service_device_id;
CREATE SEQUENCE device_id;
CREATE SEQUENCE service_action_id;
CREATE SEQUENCE service_part_id;
CREATE SEQUENCE service_type_id;
CREATE SEQUENCE invoice_id;
CREATE SEQUENCE invoice_row_id;

-- TABLES --
CREATE TABLE service_request_status_type (
  service_request_status_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT service_request_status_type_pk PRIMARY KEY (service_request_status_type)
);

CREATE TABLE so_status_type (
  so_status_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT so_status_type_pk PRIMARY KEY (so_status_type)
);

CREATE TABLE service_unit_type
(service_unit_type NUMERIC(10,0) NOT NULL,
 type_name VARCHAR(200),
  CONSTRAINT service_unit_pk PRIMARY KEY (service_unit_type)
);

CREATE TABLE service_type (
  service_type NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('service_type_id'),
  service_unit_type NUMERIC(10,0) REFERENCES service_unit_type,
  type_name VARCHAR(200),
  service_price NUMERIC,
  CONSTRAINT service_type_pk PRIMARY KEY (service_type)
);

CREATE TABLE service_action_status_type (
  service_action_status_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT service_action_status_type_pk PRIMARY KEY (service_action_status_type)
);

CREATE TABLE service_device_status_type (
  service_device_status_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT service_device_status_type_pk PRIMARY KEY (service_device_status_type)
);

CREATE TABLE invoice_status_type (
  invoice_status_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT invoice_status_type_pk PRIMARY KEY (invoice_status_type)
);

CREATE TABLE service_request (
  service_request NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('service_request_id'),
  customer NUMERIC(10,0) REFERENCES customer,
  created_by NUMERIC(10,0),
  created TIMESTAMP,
  service_desc_by_customer TEXT,
  service_desc_by_employee TEXT,
  service_request_status_type NUMERIC(10,0) REFERENCES service_request_status_type,
  CONSTRAINT service_request_pk PRIMARY KEY (service_request)
);

CREATE TABLE device_type (
  device_type NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('device_type_id'),
  super_type NUMERIC(10,0) REFERENCES device_type,
  level NUMERIC(10,0),
  type_name VARCHAR(200),
  CONSTRAINT device_type_pk PRIMARY KEY (device_type)
);

CREATE TABLE service_order (
  service_order NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('service_order_id'),
  so_status_type NUMERIC(10,0) REFERENCES so_status_type,
  created_by NUMERIC(10,0),
  service_request NUMERIC(10,0) REFERENCES service_request,
  updated_by NUMERIC(10,0),
  created TIMESTAMP,
  updated TIMESTAMP,
  status_changed TIMESTAMP,
  status_changed_by NUMERIC(10,0),
  price_total NUMERIC,
  note TEXT,
  CONSTRAINT service_order_pk PRIMARY KEY (service_order)
);

CREATE TABLE device (
  device NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('device_id'),
  device_type NUMERIC(10,0) REFERENCES device_type,
  name TEXT,
  reg_no VARCHAR(100),
  description TEXT,
  model TEXT,
  manufacturer TEXT,
  CONSTRAINT device_pk PRIMARY KEY (device)
);

CREATE TABLE service_device (
  service_device NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('service_device_id'),
  service_device_status_type NUMERIC(10,0) REFERENCES service_device_status_type,
  device NUMERIC(10,0) REFERENCES device,
  service_order NUMERIC(10,0) REFERENCES service_order,
  to_store TIMESTAMP,
  from_store TIMESTAMP,
  service_description TEXT,
  status_changed TIMESTAMP,
  store_status NUMERIC(1,0),
  CONSTRAINT service_device_pk PRIMARY KEY (service_device)
);

CREATE TABLE service_note(
  service_note NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('service_note_id'),
  customer NUMERIC(10,0) REFERENCES customer,
  employee NUMERIC(10,0) REFERENCES employee,
  service_order NUMERIC(10,0) REFERENCES service_order,
  service_device NUMERIC(10,0) REFERENCES service_device,
  note_author_type NUMERIC(10,0),
  created TIMESTAMP,
  note TEXT,
  CONSTRAINT service_note_pk PRIMARY KEY (service_note)
);

CREATE TABLE service_action (
  service_action NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('service_action_id'),
  service_action_status_type NUMERIC(10,0) REFERENCES service_action_status_type,
  service_type NUMERIC(10,0) REFERENCES service_type,
  service_device NUMERIC(10,0) REFERENCES service_device,
  service_order NUMERIC(10,0) REFERENCES service_order,
  service_amount NUMERIC,
  price NUMERIC,
  price_updated TIMESTAMP,
  action_description TEXT,
  created TIMESTAMP,
  created_by NUMERIC(10,0),
  CONSTRAINT service_action_pk PRIMARY KEY (service_action)
);

CREATE TABLE service_part(
  service_part NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('service_part_id'),
  service_order NUMERIC(10,0) REFERENCES service_order,
  service_device NUMERIC(10,0) REFERENCES service_device,
  part_name TEXT,
  serial_no TEXT,
  part_count NUMERIC(10,0),
  part_price NUMERIC,
  created TIMESTAMP,
  created_by NUMERIC(10,0),
  CONSTRAINT service_part_pk PRIMARY KEY (service_part)
);

CREATE TABLE invoice (
  invoice NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('invoice_id'),
  invoice_status_type NUMERIC(10,0) REFERENCES invoice_status_type,
  service_order NUMERIC(10,0),
  customer NUMERIC(10,0),
  invoice_date date,
  due_date date,
  price_total NUMERIC,
  receiver_name TEXT,
  reference_number TEXT,
  receiver_accounts TEXT,
  payment_date date,
  description TEXT,
  CONSTRAINT invoice_pk PRIMARY KEY (invoice)
);

CREATE TABLE invoice_row (
  invoice_row NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('invoice_row_id'),
  invoice NUMERIC(10,0) REFERENCES invoice,
  service_action NUMERIC(10,0) REFERENCES service_action,
  service_part NUMERIC(10,0) REFERENCES service_part,
  action_part_description TEXT,
  amount NUMERIC,
  price_total NUMERIC,
  unit_type VARCHAR(200),
  unit_price NUMERIC,
  invoice_row_type NUMERIC(1,0),
  CONSTRAINT invoice_row_pk PRIMARY KEY (invoice_row)
);


/* service_request tabeli indeksid ja piirangud */

CREATE INDEX service_request_idx1 ON service_request(service_request);
CREATE INDEX service_request_idx2 ON service_request(customer);
CREATE INDEX service_request_idx3 ON service_request(customer, service_desc_by_customer VARCHAR_PATTERN_OPS,service_desc_by_employee VARCHAR_PATTERN_OPS);
CREATE INDEX service_request_idx4 ON service_request(customer, created);
  

/* service_note tabeli indeksid ja piirangud */

CREATE INDEX service_note_idx1 ON service_note(service_note);
CREATE INDEX service_note_idx2 ON service_note(service_order);


/* service_order tabeli indeksid ja piirangud */

CREATE INDEX service_order_idx1 ON service_order(service_order);
CREATE INDEX service_order_idx2 ON service_order(service_request);
CREATE INDEX service_order_idx3 ON service_order(created );
CREATE INDEX service_order_idx4 ON service_order(status_changed );


/* service_device tabeli indeksid ja piirangud */

CREATE INDEX service_device_idx1 ON service_device(service_device);
CREATE INDEX service_device_idx2 ON service_device(device);
CREATE INDEX service_device_idx3 ON service_device(service_order);
CREATE INDEX service_device_idx4 ON service_device(service_device_status_type);
CREATE INDEX service_device_idx5 ON service_device(service_device_status_type, status_changed);
  
  
/* device tabeli indeksid ja piirangud */

CREATE INDEX device_idx1 ON device(device);
CREATE INDEX device_idx2 ON device(UPPER(reg_no) VARCHAR_PATTERN_OPS);
CREATE INDEX device_idx3 ON device(UPPER(description) VARCHAR_PATTERN_OPS);
CREATE INDEX device_idx4 ON device(UPPER(name) VARCHAR_PATTERN_OPS);
CREATE INDEX device_idx5 ON device(UPPER(model) VARCHAR_PATTERN_OPS);
CREATE INDEX device_idx6 ON device(UPPER(manufacturer) VARCHAR_PATTERN_OPS);


/* device_type tabeli indeksid ja piirangud */

CREATE INDEX device_type_idx1 ON device_type(device_type);
CREATE INDEX device_type_idx2 ON device_type(super_type);
CREATE INDEX device_type_idx3 ON device_type(UPPER(type_name) VARCHAR_PATTERN_OPS);


/* service_action tabeli indeksid ja piirangud */

CREATE INDEX service_action_idx1 ON service_action(service_action);
CREATE INDEX service_action_idx2 ON service_action(service_order);
CREATE INDEX service_action_idx3 ON service_action(service_device);
CREATE INDEX service_action_idx4 ON service_action(UPPER(action_description) VARCHAR_PATTERN_OPS);
CREATE INDEX service_action_idx5 ON service_action(created);
CREATE INDEX service_action_idx6 ON service_action(created_by);
CREATE INDEX service_action_idx7 ON service_action(service_action_status_type);


/* service_part tabeli indeksid ja piirangud */

CREATE INDEX service_part_idx1 ON service_part(service_part);
CREATE INDEX service_part_idx2 ON service_part(service_order);
CREATE INDEX service_part_idx3 ON service_part(created, created_by);
CREATE INDEX service_part_idx4 ON service_part(UPPER(part_name) VARCHAR_PATTERN_OPS);
CREATE INDEX service_part_idx5 ON service_part(UPPER(serial_no) VARCHAR_PATTERN_OPS);
CREATE INDEX service_part_idx6 ON service_part(service_device);


/* invoice tabeli indeksid ja piirangud */

CREATE INDEX invoice_idx1 ON invoice(invoice);
CREATE INDEX invoice_idx2 ON invoice(invoice_status_type);
CREATE INDEX invoice_idx3 ON invoice(invoice_status_type);
CREATE INDEX invoice_idx4 ON invoice(service_order);
CREATE INDEX invoice_idx5 ON invoice(invoice_date);
CREATE INDEX invoice_idx6 ON invoice(due_date);
CREATE INDEX invoice_idx7 ON invoice(customer);
CREATE INDEX invoice_idx8 ON invoice(payment_date);
CREATE INDEX invoice_idx9 ON invoice(UPPER(description) VARCHAR_PATTERN_OPS);
  
  
/* invoice_row tabeli indeksid ja piirangud */

CREATE INDEX invoice_row_idx1 ON invoice_row(invoice_row);
CREATE INDEX invoice_row_idx2 ON invoice_row(invoice);
  

/* service_type tabeli indeksid ja piirangud */

CREATE INDEX service_type_idx1 ON service_type(service_type);
CREATE INDEX service_type_idx2 ON service_type(UPPER(type_name) VARCHAR_PATTERN_OPS);