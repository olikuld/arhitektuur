-- SEQUENCES --
CREATE SEQUENCE person_id;
CREATE SEQUENCE user_account_id;
CREATE SEQUENCE customer_id;
CREATE SEQUENCE employee_role_id;
CREATE SEQUENCE employee_id;
CREATE SEQUENCE address_id;
CREATE SEQUENCE contact_id;
CREATE SEQUENCE enterprise_person_relation_id;
CREATE SEQUENCE enterprise_id;
CREATE SEQUENCE subject_attribute_id;
CREATE SEQUENCE subject_attribute_type_id;
CREATE SEQUENCE struct_unit_id;

-- TABLES --
CREATE TABLE subject_type (
  subject_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT subject_type_pk PRIMARY KEY (subject_type)
);

CREATE TABLE subject_attribute_type (
  subject_attribute_type NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('subject_attribute_type_id'),
  subject_type NUMERIC(10,0) REFERENCES subject_type,
  type_name VARCHAR(200),
  data_type NUMERIC(1,0),
  orderby NUMERIC(10,0),
  required VARCHAR(1),
  multiple_attributes VARCHAR(1),
  created_by_default VARCHAR(1),
  CONSTRAINT subject_attribute_type_pk PRIMARY KEY (subject_attribute_type)
);

CREATE TABLE employee_role_type (
  employee_role_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT employee_role_type_pk PRIMARY KEY (employee_role_type)
);

CREATE TABLE address_type (
  address_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT address_type_pk PRIMARY KEY (address_type)
);

CREATE TABLE contact_type (
  contact_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT contact_type_pk PRIMARY KEY (contact_type)
);

CREATE TABLE ent_per_relation_type (
  ent_per_relation_type NUMERIC(10,0) NOT NULL,
  type_name VARCHAR(200),
  CONSTRAINT ent_per_relation_type_pk PRIMARY KEY (ent_per_relation_type)
);

CREATE TABLE person (
  person NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('person_id'),
  first_name VARCHAR(100),
  last_name VARCHAR(100),
  identity_code VARCHAR(20),
  birth_date DATE,
  created_by NUMERIC(10,0),
  updated_by NUMERIC(10,0),
  created timestamp,
  updated timestamp,
  CONSTRAINT person_pk PRIMARY KEY (person)
);

CREATE TABLE user_account (
  user_account NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('user_account_id'),
  subject_type NUMERIC(10,0) REFERENCES subject_type,
  subject NUMERIC(10,0) REFERENCEs person,
  username VARCHAR(50),
  password VARCHAR(300),
  status NUMERIC(10,0),
  valid_from DATE,
  valid_to DATE,
  created_by NUMERIC(10,0),
  created timestamp,
  password_never_expires VARCHAR(1),
  CONSTRAINT user_account_pk PRIMARY KEY (user_account)
);

CREATE TABLE customer (
  customer NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('customer_id'),
  subject NUMERIC(10,0) REFERENCES person,
  subject_type NUMERIC(10,0) REFERENCES subject_type,
  CONSTRAINT customer_pk PRIMARY KEY (customer)
);

CREATE TABLE enterprise(
  enterprise NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('enterprise_id'),
  name TEXT,
  full_name TEXT,
  created_by NUMERIC(10,0),
  updated_by NUMERIC(10,0),
  created timestamp,
  updated timestamp,
  CONSTRAINT enterprise_pk PRIMARY KEY (enterprise)
);

CREATE TABLE struct_unit (
  struct_unit NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('struct_unit_id'),
  enterprise NUMERIC(10,0) REFERENCES enterprise,
  upper_unit_fk NUMERIC(10,0),
  level NUMERIC(10,0),
  name VARCHAR(200),
  CONSTRAINT struct_unit_pk PRIMARY KEY (struct_unit)
);

CREATE TABLE employee (
  employee NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('employee_id'),
  person NUMERIC(10,0) REFERENCES person,
  enterprise NUMERIC(10,0) REFERENCES enterprise,
  struct_unit NUMERIC(10,0) REFERENCES struct_unit,
  active VARCHAR(1),
  CONSTRAINT employee_pk PRIMARY KEY (employee)
);

CREATE TABLE employee_role (
  employee_role NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('employee_role_id'),
  employee NUMERIC(10,0) REFERENCES employee,
  employee_role_type NUMERIC(10,0) REFERENCES employee_role_type,
  active VARCHAR(1),
  CONSTRAINT employee_role_pk PRIMARY KEY (employee_role)
);

CREATE TABLE address (
  address NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('address_id'),
  address_type NUMERIC(10,0) REFERENCES address_type,
  subject NUMERIC(10,0) REFERENCES person,
  subject_type NUMERIC(10,0) REFERENCES subject_type,
  country VARCHAR(50),
  county VARCHAR(100),
  town_village VARCHAR(100),
  street_address VARCHAR(100),
  zipcode VARCHAR(50),
  CONSTRAINT address_pk PRIMARY KEY (address)
);

CREATE TABLE contact (
  contact NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('contact_id'),
  subject NUMERIC(10,0) REFERENCES person,
  contact_type NUMERIC(10,0) REFERENCES contact_type,
  value_text TEXT,
  orderby NUMERIC(10,0),
  subject_type NUMERIC(10,0) REFERENCES subject_type,
  note TEXT,
  CONSTRAINT contact_pk PRIMARY KEY (contact)
);

CREATE TABLE enterprise_person_relation(
  enterprise_person_relation NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('enterprise_person_relation_id'),
  person NUMERIC(10,0) REFERENCES person,
  enterprise NUMERIC(10,0) REFERENCES enterprise,
  ent_per_relation_type NUMERIC(10,0) REFERENCES ent_per_relation_type,
  CONSTRAINT enterprise_person_relation_pk PRIMARY KEY (enterprise_person_relation)
);

CREATE TABLE subject_attribute (
  subject_attribute NUMERIC(10,0) NOT NULL DEFAULT NEXTVAL('subject_attribute_id'),
  subject NUMERIC(10,0) REFERENCES person,
  subject_attribute_type NUMERIC(10,0) REFERENCES subject_attribute_type,
  subject_type NUMERIC(10,0) REFERENCES subject_type,
  orderby NUMERIC(10,0),
  value_text TEXT,
  value_number NUMERIC,
  value_date DATE,
  data_type NUMERIC(1,0),
  CONSTRAINT subject_attribute_pk PRIMARY KEY (subject_attribute)
);

/* person tabeli indeksid ja piirangud */

CREATE INDEX person_idx1 ON person(person);
  
CREATE INDEX person_idx2 ON person(UPPER(first_name) VARCHAR_PATTERN_OPS);
CREATE INDEX person_idx3 ON person(UPPER(last_name) VARCHAR_PATTERN_OPS);

CREATE INDEX person_idx4 ON person(identity_code VARCHAR_PATTERN_OPS);
CREATE INDEX person_idx5 ON person(birth_date);
CREATE INDEX person_idx6 ON person(created);
CREATE INDEX person_idx7 ON person(created_by);


/* user_account tabeli indeksid ja piirangud */

CREATE INDEX user_account_idx1 ON user_account(user_account);
CREATE INDEX user_account_idx2 ON user_account(username VARCHAR_PATTERN_OPS, password VARCHAR_PATTERN_OPS, status);
CREATE INDEX user_account_idx3 ON user_account(subject_type);
CREATE INDEX user_account_idx4 ON user_account(subject);


/* customer tabeli indeksid ja piirangud */

CREATE INDEX customer_idx1 ON customer(customer);
CREATE INDEX customer_idx2 ON customer(subject);
CREATE INDEX customer_idx3 ON customer(subject_type);


/* employee_role  tabeli indeksid ja piirangud */

CREATE INDEX employee_role_idx1 ON employee_role(employee_role);
CREATE INDEX employee_role_idx2 ON employee_role(employee);
CREATE INDEX employee_role_idx3 ON employee_role(employee_role_type);


/* employee  tabeli indeksid ja piirangud */

CREATE INDEX employee_idx1 ON employee(employee);
CREATE INDEX employee_idx2 ON employee(person);
CREATE INDEX employee_idx3 ON employee(enterprise);
CREATE INDEX employee_idx4 ON employee(struct_unit);


/* address  tabeli indeksid ja piirangud */

CREATE INDEX address_idx1 ON address(address);
CREATE INDEX address_idx2 ON address(address_type);
CREATE INDEX address_idx3 ON address(subject_type);
CREATE INDEX address_idx4 ON address(UPPER(country) VARCHAR_PATTERN_OPS);
CREATE INDEX address_idx5 ON address(UPPER(county) VARCHAR_PATTERN_OPS);
CREATE INDEX address_idx6 ON address(UPPER(town_village) VARCHAR_PATTERN_OPS);
CREATE INDEX address_idx7 ON address(UPPER(street_address) VARCHAR_PATTERN_OPS);
CREATE INDEX address_idx8 ON address(UPPER(zipcode) VARCHAR_PATTERN_OPS);


/* contact  tabeli indeksid ja piirangud */

CREATE INDEX contact_idx1 ON contact(contact);
CREATE INDEX contact_idx2 ON contact(subject);
CREATE INDEX contact_idx3 ON contact(subject_type);
CREATE INDEX contact_idx4 ON contact(contact_type);
CREATE INDEX contact_idx5 ON contact(UPPER(value_text) VARCHAR_PATTERN_OPS);


/* enterprise_person_relation  tabeli indeksid ja piirangud */  

CREATE INDEX enterprise_person_relation_idx1 ON enterprise_person_relation(enterprise_person_relation);
CREATE INDEX enterprise_person_relation_idx2 ON enterprise_person_relation(person);
CREATE INDEX enterprise_person_relation_idx3 ON enterprise_person_relation(enterprise);
CREATE INDEX enterprise_person_relation_idx4 ON enterprise_person_relation(ent_per_relation_type);


/* enterprise  tabeli indeksid ja piirangud */  

CREATE INDEX enterprise_idx1 ON enterprise(enterprise);
CREATE INDEX enterprise_idx2 ON enterprise(UPPER(name) VARCHAR_PATTERN_OPS);
CREATE INDEX enterprise_idx3 ON enterprise(UPPER(full_name) VARCHAR_PATTERN_OPS);


/* subject_attribute  tabeli indeksid ja piirangud */  

CREATE INDEX subject_attribute_idx1 ON subject_attribute(subject_attribute);CREATE INDEX subject_attribute_idx2 ON subject_attribute(subject);
CREATE INDEX subject_attribute_idx5 ON subject_attribute(subject_attribute_type);
CREATE INDEX subject_attribute_idx6 ON subject_attribute(subject_type);
CREATE INDEX subject_attribute_idx7 ON subject_attribute(UPPER(value_text) VARCHAR_PATTERN_OPS);
CREATE INDEX subject_attribute_idx8 ON subject_attribute(value_number);
CREATE INDEX subject_attribute_idx9 ON subject_attribute(value_date);


/* subject_attribute_type  tabeli indeksid ja piirangud */    

CREATE INDEX subject_attribute_type_idx1 ON subject_attribute_type(subject_attribute_type);
CREATE INDEX subject_attribute_type_idx2 ON subject_attribute_type(subject_type);
CREATE INDEX subject_attribute_type_idx3 ON subject_attribute_type(UPPER(type_name) VARCHAR_PATTERN_OPS);


/* struct_unit  tabeli indeksid ja piirangud */    

CREATE INDEX struct_unit_idx1 ON struct_unit(struct_unit);
CREATE INDEX struct_unit_idx2 ON struct_unit(enterprise);
CREATE INDEX struct_unit_idx3 ON struct_unit(upper_unit_fk);
CREATE INDEX struct_unit_idx4 ON struct_unit(UPPER(name) VARCHAR_PATTERN_OPS);