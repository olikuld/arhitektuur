package com.ttu.model.subject;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "ent_per_relation_type", schema = "public", catalog = "repair_service")
public class EntPerRelationType {
    private int entPerRelationType;
    private String typeName;

    @Id
    @Column(name = "ent_per_relation_type")
    public int getEntPerRelationType() {
        return entPerRelationType;
    }

    public void setEntPerRelationType(int entPerRelationType) {
        this.entPerRelationType = entPerRelationType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntPerRelationType that = (EntPerRelationType) o;

        if (entPerRelationType != that.entPerRelationType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = entPerRelationType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
