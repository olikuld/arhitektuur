package com.ttu.model.subject;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "employee_role_type", schema = "public", catalog = "repair_service")
public class EmployeeRoleType {
    private int employeeRoleType;
    private String typeName;

    @Id
    @Column(name = "employee_role_type")
    public int getEmployeeRoleType() {
        return employeeRoleType;
    }

    public void setEmployeeRoleType(int employeeRoleType) {
        this.employeeRoleType = employeeRoleType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmployeeRoleType that = (EmployeeRoleType) o;

        if (employeeRoleType != that.employeeRoleType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = employeeRoleType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
