package com.ttu.model.subject;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "struct_unit", schema = "public", catalog = "repair_service")
public class StructUnit {
    private int structUnit;
    private Enterprise enterprise;
    private Integer upperUnitFk;
    private Integer level;
    private String name;

    @Id
    @Column(name = "struct_unit")
    public int getStructUnit() {
        return structUnit;
    }

    public void setStructUnit(int structUnit) {
        this.structUnit = structUnit;
    }

    @ManyToOne
    @JoinColumn(name = "enterprise")
    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    @Basic
    @Column(name = "upper_unit_fk")
    public Integer getUpperUnitFk() {
        return upperUnitFk;
    }

    public void setUpperUnitFk(Integer upperUnitFk) {
        this.upperUnitFk = upperUnitFk;
    }

    @Basic
    @Column(name = "level")
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StructUnit that = (StructUnit) o;

        if (structUnit != that.structUnit) return false;
        if (enterprise != null ? !enterprise.equals(that.enterprise) : that.enterprise != null) return false;
        if (upperUnitFk != null ? !upperUnitFk.equals(that.upperUnitFk) : that.upperUnitFk != null) return false;
        if (level != null ? !level.equals(that.level) : that.level != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = structUnit;
        result = 31 * result + (enterprise != null ? enterprise.hashCode() : 0);
        result = 31 * result + (upperUnitFk != null ? upperUnitFk.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
