package com.ttu.model.subject;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.repairService.ServiceDevice;
import com.ttu.model.repairService.ServiceRequest;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Customer {
    private int customer;
    private Person subject;
    private Integer subjectType;
    private List<ServiceRequest> serviceRequests = new ArrayList<>();

    @Id
    @Column(name = "customer")
    public int getCustomer() {
        return customer;
    }

    public void setCustomer(int customer) {
        this.customer = customer;
    }


    @ManyToOne
    @JoinColumn(name = "subject")
    public Person getSubject() {
        return subject;
    }

    public void setSubject(Person subjectFk) {
        this.subject = subjectFk;
    }

    @Basic
    @Column(name = "subject_type")
    public Integer getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer1 = (Customer) o;

        if (customer != customer1.customer) return false;
        if (subject != null ? !subject.equals(customer1.subject) : customer1.subject != null) return false;
        if (subjectType != null ? !subjectType.equals(customer1.subjectType) : customer1.subjectType != null)
            return false;

        return true;
    }

    @Valid
    @OrderBy("serviceRequest")
    @OneToMany(targetEntity = ServiceRequest.class, mappedBy = "customer")
    public List<ServiceRequest> getServiceRequests() {
        return serviceRequests;
    }

    public void setServiceRequests(List<ServiceRequest> serviceRequests) {
        this.serviceRequests = serviceRequests;
    }

    @Override
    public int hashCode() {
        int result = customer;
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (subjectType != null ? subjectType.hashCode() : 0);
        return result;
    }
}
