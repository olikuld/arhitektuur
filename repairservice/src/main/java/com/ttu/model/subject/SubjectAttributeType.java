package com.ttu.model.subject;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "subject_attribute_type", schema = "public", catalog = "repair_service")
public class SubjectAttributeType {
    private int subjectAttributeType;
    private Integer subjectType;
    private String typeName;
    private BigInteger dataType;
    private Integer orderby;
    private String required;
    private String multipleAttributes;
    private String createdByDefault;

    @Id
    @Column(name = "subject_attribute_type")
    public int getSubjectAttributeType() {
        return subjectAttributeType;
    }

    public void setSubjectAttributeType(int subjectAttributeType) {
        this.subjectAttributeType = subjectAttributeType;
    }

    @Basic
    @Column(name = "subject_type")
    public Integer getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Basic
    @Column(name = "data_type")
    public BigInteger getDataType() {
        return dataType;
    }

    public void setDataType(BigInteger dataType) {
        this.dataType = dataType;
    }

    @Basic
    @Column(name = "orderby")
    public Integer getOrderby() {
        return orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }

    @Basic
    @Column(name = "required")
    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    @Basic
    @Column(name = "multiple_attributes")
    public String getMultipleAttributes() {
        return multipleAttributes;
    }

    public void setMultipleAttributes(String multipleAttributes) {
        this.multipleAttributes = multipleAttributes;
    }

    @Basic
    @Column(name = "created_by_default")
    public String getCreatedByDefault() {
        return createdByDefault;
    }

    public void setCreatedByDefault(String createdByDefault) {
        this.createdByDefault = createdByDefault;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectAttributeType that = (SubjectAttributeType) o;

        if (subjectAttributeType != that.subjectAttributeType) return false;
        if (subjectType != null ? !subjectType.equals(that.subjectType) : that.subjectType != null) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;
        if (dataType != null ? !dataType.equals(that.dataType) : that.dataType != null) return false;
        if (orderby != null ? !orderby.equals(that.orderby) : that.orderby != null) return false;
        if (required != null ? !required.equals(that.required) : that.required != null) return false;
        if (multipleAttributes != null ? !multipleAttributes.equals(that.multipleAttributes) : that.multipleAttributes != null)
            return false;
        if (createdByDefault != null ? !createdByDefault.equals(that.createdByDefault) : that.createdByDefault != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = subjectAttributeType;
        result = 31 * result + (subjectType != null ? subjectType.hashCode() : 0);
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        result = 31 * result + (orderby != null ? orderby.hashCode() : 0);
        result = 31 * result + (required != null ? required.hashCode() : 0);
        result = 31 * result + (multipleAttributes != null ? multipleAttributes.hashCode() : 0);
        result = 31 * result + (createdByDefault != null ? createdByDefault.hashCode() : 0);
        return result;
    }
}
