package com.ttu.model.subject;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "contact_type", schema = "public", catalog = "repair_service")
public class ContactType {
    private int contactType;
    private String typeName;

    @Id
    @Column(name = "contact_type")
    public int getContactType() {
        return contactType;
    }

    public void setContactType(int contactType) {
        this.contactType = contactType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactType that = (ContactType) o;

        if (contactType != that.contactType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = contactType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
