package com.ttu.model.subject;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Table(name = "subject_attribute", schema = "public", catalog = "repair_service")
public class SubjectAttribute {
    private int subjectAttribute;
    private Person subject;
    private Integer subjectAttributeType;
    private Integer subjectType;
    private Integer orderby;
    private String valueText;
    private BigInteger valueNumber;
    private Date valueDate;
    private BigInteger dataType;

    @Id
    @Column(name = "subject_attribute")
    public int getSubjectAttribute() {
        return subjectAttribute;
    }

    public void setSubjectAttribute(int subjectAttribute) {
        this.subjectAttribute = subjectAttribute;
    }


    @ManyToOne
    @JoinColumn(name = "subject")
    public Person getSubject() {
        return subject;
    }

    public void setSubject(Person subject) {
        this.subject = subject;
    }

    @Basic
    @Column(name = "subject_attribute_type")
    public Integer getSubjectAttributeType() {
        return subjectAttributeType;
    }

    public void setSubjectAttributeType(Integer subjectAttributeType) {
        this.subjectAttributeType = subjectAttributeType;
    }

    @Basic
    @Column(name = "subject_type")
    public Integer getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    @Basic
    @Column(name = "orderby")
    public Integer getOrderby() {
        return orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }

    @Basic
    @Column(name = "value_text")
    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText;
    }

    @Basic
    @Column(name = "value_number")
    public BigInteger getValueNumber() {
        return valueNumber;
    }

    public void setValueNumber(BigInteger valueNumber) {
        this.valueNumber = valueNumber;
    }

    @Basic
    @Column(name = "value_date")
    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    @Basic
    @Column(name = "data_type")
    public BigInteger getDataType() {
        return dataType;
    }

    public void setDataType(BigInteger dataType) {
        this.dataType = dataType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectAttribute that = (SubjectAttribute) o;

        if (subjectAttribute != that.subjectAttribute) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        if (subjectAttributeType != null ? !subjectAttributeType.equals(that.subjectAttributeType) : that.subjectAttributeType != null)
            return false;
        if (subjectType != null ? !subjectType.equals(that.subjectType) : that.subjectType != null) return false;
        if (orderby != null ? !orderby.equals(that.orderby) : that.orderby != null) return false;
        if (valueText != null ? !valueText.equals(that.valueText) : that.valueText != null) return false;
        if (valueNumber != null ? !valueNumber.equals(that.valueNumber) : that.valueNumber != null) return false;
        if (valueDate != null ? !valueDate.equals(that.valueDate) : that.valueDate != null) return false;
        if (dataType != null ? !dataType.equals(that.dataType) : that.dataType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = subjectAttribute;
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (subjectAttributeType != null ? subjectAttributeType.hashCode() : 0);
        result = 31 * result + (subjectType != null ? subjectType.hashCode() : 0);
        result = 31 * result + (orderby != null ? orderby.hashCode() : 0);
        result = 31 * result + (valueText != null ? valueText.hashCode() : 0);
        result = 31 * result + (valueNumber != null ? valueNumber.hashCode() : 0);
        result = 31 * result + (valueDate != null ? valueDate.hashCode() : 0);
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        return result;
    }
}
