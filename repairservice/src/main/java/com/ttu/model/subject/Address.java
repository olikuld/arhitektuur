package com.ttu.model.subject;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
public class Address {
    private int address;
    private Integer addressType;
    private Person subject;
    private Integer subjectType;
    private String country;
    private String county;
    private String townVillage;
    private String streetAddress;
    private String zipcode;

    @Id
    @Column(name = "address")
    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    @Basic
    @Column(name = "address_type")
    public Integer getAddressType() {
        return addressType;
    }

    public void setAddressType(Integer addressType) {
        this.addressType = addressType;
    }

    @ManyToOne
    @JoinColumn(name = "subject")
    public Person getSubject() {
        return subject;
    }

    public void setSubject(Person subject) {
        this.subject = subject;
    }

    @Basic
    @Column(name = "subject_type")
    public Integer getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    @Basic
    @Column(name = "country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "county")
    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    @Basic
    @Column(name = "town_village")
    public String getTownVillage() {
        return townVillage;
    }

    public void setTownVillage(String townVillage) {
        this.townVillage = townVillage;
    }

    @Basic
    @Column(name = "street_address")
    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    @Basic
    @Column(name = "zipcode")
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address1 = (Address) o;

        if (address != address1.address) return false;
        if (addressType != null ? !addressType.equals(address1.addressType) : address1.addressType != null)
            return false;
        if (subject != null ? !subject.equals(address1.subject) : address1.subject != null) return false;
        if (subjectType != null ? !subjectType.equals(address1.subjectType) : address1.subjectType != null)
            return false;
        if (country != null ? !country.equals(address1.country) : address1.country != null) return false;
        if (county != null ? !county.equals(address1.county) : address1.county != null) return false;
        if (townVillage != null ? !townVillage.equals(address1.townVillage) : address1.townVillage != null)
            return false;
        if (streetAddress != null ? !streetAddress.equals(address1.streetAddress) : address1.streetAddress != null)
            return false;
        if (zipcode != null ? !zipcode.equals(address1.zipcode) : address1.zipcode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = address;
        result = 31 * result + (addressType != null ? addressType.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (subjectType != null ? subjectType.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (county != null ? county.hashCode() : 0);
        result = 31 * result + (townVillage != null ? townVillage.hashCode() : 0);
        result = 31 * result + (streetAddress != null ? streetAddress.hashCode() : 0);
        result = 31 * result + (zipcode != null ? zipcode.hashCode() : 0);
        return result;
    }
}
