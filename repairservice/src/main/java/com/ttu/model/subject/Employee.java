package com.ttu.model.subject;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
public class Employee {
    private int employee;
    private Person person;
    private Enterprise enterprise;
    private StructUnit structUnit;
    private String active;

    @Id
    @Column(name = "employee")
    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    @ManyToOne
    @JoinColumn(name = "person")
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @ManyToOne
    @JoinColumn(name = "enterprise")
    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    @ManyToOne
    @JoinColumn(name = "struct_unit")
    public StructUnit getStructUnit() {
        return structUnit;
    }

    public void setStructUnit(StructUnit structUnit) {
        this.structUnit = structUnit;
    }

    @Basic
    @Column(name = "active")
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee1 = (Employee) o;

        if (employee != employee1.employee) return false;
        if (person != null ? !person.equals(employee1.person) : employee1.person != null) return false;
        if (enterprise != null ? !enterprise.equals(employee1.enterprise) : employee1.enterprise != null) return false;
        if (structUnit != null ? !structUnit.equals(employee1.structUnit) : employee1.structUnit != null) return false;
        if (active != null ? !active.equals(employee1.active) : employee1.active != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = employee;
        result = 31 * result + (person != null ? person.hashCode() : 0);
        result = 31 * result + (enterprise != null ? enterprise.hashCode() : 0);
        result = 31 * result + (structUnit != null ? structUnit.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        return result;
    }
}
