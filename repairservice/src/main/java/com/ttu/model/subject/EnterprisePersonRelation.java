package com.ttu.model.subject;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "enterprise_person_relation", schema = "public", catalog = "repair_service")
public class EnterprisePersonRelation {
    private int enterprisePersonRelation;
    private Person person;
    private Enterprise enterprise;
    private Integer entPerRelationType;

    @Id
    @Column(name = "enterprise_person_relation")
    public int getEnterprisePersonRelation() {
        return enterprisePersonRelation;
    }

    public void setEnterprisePersonRelation(int enterprisePersonRelation) {
        this.enterprisePersonRelation = enterprisePersonRelation;
    }

    @ManyToOne
    @JoinColumn(name = "person")
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @ManyToOne
    @JoinColumn(name = "enterprise")
    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    @Basic
    @Column(name = "ent_per_relation_type")
    public Integer getEntPerRelationType() {
        return entPerRelationType;
    }

    public void setEntPerRelationType(Integer entPerRelationType) {
        this.entPerRelationType = entPerRelationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EnterprisePersonRelation that = (EnterprisePersonRelation) o;

        if (enterprisePersonRelation != that.enterprisePersonRelation) return false;
        if (person != null ? !person.equals(that.person) : that.person != null) return false;
        if (enterprise != null ? !enterprise.equals(that.enterprise) : that.enterprise != null) return false;
        if (entPerRelationType != null ? !entPerRelationType.equals(that.entPerRelationType) : that.entPerRelationType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = enterprisePersonRelation;
        result = 31 * result + (person != null ? person.hashCode() : 0);
        result = 31 * result + (enterprise != null ? enterprise.hashCode() : 0);
        result = 31 * result + (entPerRelationType != null ? entPerRelationType.hashCode() : 0);
        return result;
    }
}
