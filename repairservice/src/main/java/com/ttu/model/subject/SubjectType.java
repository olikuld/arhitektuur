package com.ttu.model.subject;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "subject_type", schema = "public", catalog = "repair_service")
public class SubjectType {
    private int subjectType;
    private String typeName;

    @Id
    @Column(name = "subject_type")
    public int getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(int subjectType) {
        this.subjectType = subjectType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectType that = (SubjectType) o;

        if (subjectType != that.subjectType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = subjectType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
