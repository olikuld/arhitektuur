package com.ttu.model.subject;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "address_type", schema = "public", catalog = "repair_service")
public class AddressType {
    private int addressType;
    private String typeName;

    @Id
    @Column(name = "address_type")
    public int getAddressType() {
        return addressType;
    }

    public void setAddressType(int addressType) {
        this.addressType = addressType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressType that = (AddressType) o;

        if (addressType != that.addressType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = addressType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
