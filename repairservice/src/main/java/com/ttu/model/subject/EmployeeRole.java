package com.ttu.model.subject;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "employee_role", schema = "public", catalog = "repair_service")
public class EmployeeRole {
    private int employeeRole;
    private Employee employee;
    private Integer employeeRoleType;
    private String active;

    @Id
    @Column(name = "employee_role")
    public int getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(int employeeRole) {
        this.employeeRole = employeeRole;
    }

    @ManyToOne
    @JoinColumn(name = "employee")
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Basic
    @Column(name = "employee_role_type")
    public Integer getEmployeeRoleType() {
        return employeeRoleType;
    }

    public void setEmployeeRoleType(Integer employeeRoleType) {
        this.employeeRoleType = employeeRoleType;
    }

    @Basic
    @Column(name = "active")
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmployeeRole that = (EmployeeRole) o;

        if (employeeRole != that.employeeRole) return false;
        if (employee != null ? !employee.equals(that.employee) : that.employee != null) return false;
        if (employeeRoleType != null ? !employeeRoleType.equals(that.employeeRoleType) : that.employeeRoleType != null)
            return false;
        if (active != null ? !active.equals(that.active) : that.active != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = employeeRole;
        result = 31 * result + (employee != null ? employee.hashCode() : 0);
        result = 31 * result + (employeeRoleType != null ? employeeRoleType.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        return result;
    }
}
