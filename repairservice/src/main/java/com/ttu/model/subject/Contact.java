package com.ttu.model.subject;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
public class Contact {
    private int contact;
    private Person subject;
    private Integer contactType;
    private String valueText;
    private Integer orderby;
    private Integer subjectType;
    private String note;

    @Id
    @Column(name = "contact")
    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    @ManyToOne
    @JoinColumn(name = "subject")
    public Person getSubject() {
        return subject;
    }

    public void setSubject(Person subject) {
        this.subject = subject;
    }

    @Basic
    @Column(name = "contact_type")
    public Integer getContactType() {
        return contactType;
    }

    public void setContactType(Integer contactType) {
        this.contactType = contactType;
    }

    @Basic
    @Column(name = "value_text")
    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText;
    }

    @Basic
    @Column(name = "orderby")
    public Integer getOrderby() {
        return orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }

    @Basic
    @Column(name = "subject_type")
    public Integer getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    @Basic
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact1 = (Contact) o;

        if (contact != contact1.contact) return false;
        if (subject != null ? !subject.equals(contact1.subject) : contact1.subject != null) return false;
        if (contactType != null ? !contactType.equals(contact1.contactType) : contact1.contactType != null)
            return false;
        if (valueText != null ? !valueText.equals(contact1.valueText) : contact1.valueText != null) return false;
        if (orderby != null ? !orderby.equals(contact1.orderby) : contact1.orderby != null) return false;
        if (subjectType != null ? !subjectType.equals(contact1.subjectType) : contact1.subjectType != null)
            return false;
        if (note != null ? !note.equals(contact1.note) : contact1.note != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = contact;
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (contactType != null ? contactType.hashCode() : 0);
        result = 31 * result + (valueText != null ? valueText.hashCode() : 0);
        result = 31 * result + (orderby != null ? orderby.hashCode() : 0);
        result = 31 * result + (subjectType != null ? subjectType.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }
}
