package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.subject.Customer;

import javax.persistence.*;
import javax.validation.Valid;
import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Invoice {
    private int invoice;
    private InvoiceStatusType invoiceStatusType;
    private ServiceOrder serviceOrder;
    private List<InvoiceRow> invoiceRows = new ArrayList<>();
    private Customer customer;
    private Date invoiceDate;
    private Date dueDate;
    private BigInteger priceTotal;
    private String receiverName;
    private String referenceNumber;
    private String receiverAccounts;
    private Date paymentDate;
    private String description;

    @Id
    @SequenceGenerator(name = "invoice_id", sequenceName = "invoice_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoice_id")
    @Column(name = "invoice")
    public int getInvoice() {
        return invoice;
    }

    public void setInvoice(int invoice) {
        this.invoice = invoice;
    }

    @ManyToOne
    @JoinColumn(name = "invoice_status_type")
    public InvoiceStatusType getInvoiceStatusType() {
        return invoiceStatusType;
    }

    public void setInvoiceStatusType(InvoiceStatusType invoiceStatusType) {
        this.invoiceStatusType = invoiceStatusType;
    }

    @OneToOne
    @JoinColumn(name = "service_order")
    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(ServiceOrder serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    @ManyToOne
    @JoinColumn(name = "customer")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Valid
    @OrderBy("invoiceRow")
    @OneToMany(targetEntity = InvoiceRow.class, cascade = CascadeType.REMOVE, mappedBy = "invoice")
    public List<InvoiceRow> getInvoiceRows() {
        return invoiceRows;
    }

    public void setInvoiceRows(List<InvoiceRow> invoiceRows) {
        this.invoiceRows = invoiceRows;
    }

    @Basic
    @Column(name = "invoice_date")
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Basic
    @Column(name = "due_date")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Basic
    @Column(name = "price_total")
    public BigInteger getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(BigInteger priceTotal) {
        this.priceTotal = priceTotal;
    }

    @Basic
    @Column(name = "receiver_name")
    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    @Basic
    @Column(name = "reference_number")
    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    @Basic
    @Column(name = "receiver_accounts")
    public String getReceiverAccounts() {
        return receiverAccounts;
    }

    public void setReceiverAccounts(String receiverAccounts) {
        this.receiverAccounts = receiverAccounts;
    }

    @Basic
    @Column(name = "payment_date")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Invoice invoice1 = (Invoice) o;

        if (invoice != invoice1.invoice) return false;
        if (invoiceStatusType != null ? !invoiceStatusType.equals(invoice1.invoiceStatusType) : invoice1.invoiceStatusType != null)
            return false;
        if (serviceOrder != null ? !serviceOrder.equals(invoice1.serviceOrder) : invoice1.serviceOrder != null)
            return false;
        if (customer != null ? !customer.equals(invoice1.customer) : invoice1.customer != null) return false;
        if (invoiceDate != null ? !invoiceDate.equals(invoice1.invoiceDate) : invoice1.invoiceDate != null)
            return false;
        if (dueDate != null ? !dueDate.equals(invoice1.dueDate) : invoice1.dueDate != null) return false;
        if (priceTotal != null ? !priceTotal.equals(invoice1.priceTotal) : invoice1.priceTotal != null) return false;
        if (receiverName != null ? !receiverName.equals(invoice1.receiverName) : invoice1.receiverName != null)
            return false;
        if (referenceNumber != null ? !referenceNumber.equals(invoice1.referenceNumber) : invoice1.referenceNumber != null)
            return false;
        if (receiverAccounts != null ? !receiverAccounts.equals(invoice1.receiverAccounts) : invoice1.receiverAccounts != null)
            return false;
        if (paymentDate != null ? !paymentDate.equals(invoice1.paymentDate) : invoice1.paymentDate != null)
            return false;
        if (description != null ? !description.equals(invoice1.description) : invoice1.description != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = invoice;
        result = 31 * result + (invoiceStatusType != null ? invoiceStatusType.hashCode() : 0);
        result = 31 * result + (serviceOrder != null ? serviceOrder.hashCode() : 0);
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (invoiceDate != null ? invoiceDate.hashCode() : 0);
        result = 31 * result + (dueDate != null ? dueDate.hashCode() : 0);
        result = 31 * result + (priceTotal != null ? priceTotal.hashCode() : 0);
        result = 31 * result + (receiverName != null ? receiverName.hashCode() : 0);
        result = 31 * result + (referenceNumber != null ? referenceNumber.hashCode() : 0);
        result = 31 * result + (receiverAccounts != null ? receiverAccounts.hashCode() : 0);
        result = 31 * result + (paymentDate != null ? paymentDate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
