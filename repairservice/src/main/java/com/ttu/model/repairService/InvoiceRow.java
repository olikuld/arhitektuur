package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "invoice_row", schema = "public", catalog = "repair_service")
public class InvoiceRow {
    private int invoiceRow;
    private Invoice invoice;
    private ServiceAction serviceAction;
    private ServicePart servicePart;
    private String actionPartDescription;
    private BigInteger amount;
    private BigInteger priceTotal;
    private String unitType;
    private BigInteger unitPrice;
    private BigInteger invoiceRowType;

    @Id
    @SequenceGenerator(name = "invoice_row_id", sequenceName = "invoice_row_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoice_row_id")
    @Column(name = "invoice_row")
    public int getInvoiceRow() {
        return invoiceRow;
    }

    public void setInvoiceRow(int invoiceRow) {
        this.invoiceRow = invoiceRow;
    }

    @ManyToOne
    @JoinColumn(name = "invoice")
    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    @ManyToOne
    @JoinColumn(name = "service_action")
    public ServiceAction getServiceAction() {
        return serviceAction;
    }

    public void setServiceAction(ServiceAction serviceAction) {
        this.serviceAction = serviceAction;
    }

    @ManyToOne
    @JoinColumn(name = "service_part")
    public ServicePart getServicePart() {
        return servicePart;
    }

    public void setServicePart(ServicePart servicePart) {
        this.servicePart = servicePart;
    }

    @Basic
    @Column(name = "action_part_description")
    public String getActionPartDescription() {
        return actionPartDescription;
    }

    public void setActionPartDescription(String actionPartDescription) {
        this.actionPartDescription = actionPartDescription;
    }

    @Basic
    @Column(name = "amount")
    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "price_total")
    public BigInteger getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(BigInteger priceTotal) {
        this.priceTotal = priceTotal;
    }

    @Basic
    @Column(name = "unit_type")
    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    @Basic
    @Column(name = "unit_price")
    public BigInteger getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigInteger unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "invoice_row_type")
    public BigInteger getInvoiceRowType() {
        return invoiceRowType;
    }

    public void setInvoiceRowType(BigInteger invoiceRowType) {
        this.invoiceRowType = invoiceRowType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvoiceRow that = (InvoiceRow) o;

        if (invoiceRow != that.invoiceRow) return false;
        if (invoice != null ? !invoice.equals(that.invoice) : that.invoice != null) return false;
        if (serviceAction != null ? !serviceAction.equals(that.serviceAction) : that.serviceAction != null)
            return false;
        if (servicePart != null ? !servicePart.equals(that.servicePart) : that.servicePart != null) return false;
        if (actionPartDescription != null ? !actionPartDescription.equals(that.actionPartDescription) : that.actionPartDescription != null)
            return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (priceTotal != null ? !priceTotal.equals(that.priceTotal) : that.priceTotal != null) return false;
        if (unitType != null ? !unitType.equals(that.unitType) : that.unitType != null) return false;
        if (unitPrice != null ? !unitPrice.equals(that.unitPrice) : that.unitPrice != null) return false;
        if (invoiceRowType != null ? !invoiceRowType.equals(that.invoiceRowType) : that.invoiceRowType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = invoiceRow;
        result = 31 * result + (invoice != null ? invoice.hashCode() : 0);
        result = 31 * result + (serviceAction != null ? serviceAction.hashCode() : 0);
        result = 31 * result + (servicePart != null ? servicePart.hashCode() : 0);
        result = 31 * result + (actionPartDescription != null ? actionPartDescription.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (priceTotal != null ? priceTotal.hashCode() : 0);
        result = 31 * result + (unitType != null ? unitType.hashCode() : 0);
        result = 31 * result + (unitPrice != null ? unitPrice.hashCode() : 0);
        result = 31 * result + (invoiceRowType != null ? invoiceRowType.hashCode() : 0);
        return result;
    }
}
