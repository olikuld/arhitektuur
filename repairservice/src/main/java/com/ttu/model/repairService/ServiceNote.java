package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.subject.Customer;
import com.ttu.model.subject.Employee;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "service_note", schema = "public", catalog = "repair_service")
public class ServiceNote {
    private int serviceNote;
    private Customer customer;
    private Employee employee;
    private ServiceOrder serviceOrder;
    private ServiceDevice serviceDevice;
    private Integer noteAuthorType;
    private Timestamp created;
    private String note;

    @Id
    @Column(name = "service_note")
    public int getServiceNote() {
        return serviceNote;
    }

    public void setServiceNote(int serviceNote) {
        this.serviceNote = serviceNote;
    }

    @ManyToOne
    @JoinColumn(name = "customer")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @ManyToOne
    @JoinColumn(name = "employee")
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @ManyToOne
    @JoinColumn(name = "service_order")
    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(ServiceOrder serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    @ManyToOne
    @JoinColumn(name = "service_device")
    public ServiceDevice getServiceDevice() {
        return serviceDevice;
    }

    public void setServiceDevice(ServiceDevice serviceDevice) {
        this.serviceDevice = serviceDevice;
    }

    @Basic
    @Column(name = "note_author_type")
    public Integer getNoteAuthorType() {
        return noteAuthorType;
    }

    public void setNoteAuthorType(Integer noteAuthorType) {
        this.noteAuthorType = noteAuthorType;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceNote that = (ServiceNote) o;

        if (serviceNote != that.serviceNote) return false;
        if (customer != null ? !customer.equals(that.customer) : that.customer != null) return false;
        if (employee != null ? !employee.equals(that.employee) : that.employee != null) return false;
        if (serviceOrder != null ? !serviceOrder.equals(that.serviceOrder) : that.serviceOrder != null) return false;
        if (serviceDevice != null ? !serviceDevice.equals(that.serviceDevice) : that.serviceDevice != null)
            return false;
        if (noteAuthorType != null ? !noteAuthorType.equals(that.noteAuthorType) : that.noteAuthorType != null)
            return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceNote;
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (employee != null ? employee.hashCode() : 0);
        result = 31 * result + (serviceOrder != null ? serviceOrder.hashCode() : 0);
        result = 31 * result + (serviceDevice != null ? serviceDevice.hashCode() : 0);
        result = 31 * result + (noteAuthorType != null ? noteAuthorType.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }
}
