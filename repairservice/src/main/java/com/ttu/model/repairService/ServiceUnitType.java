package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "service_unit_type", schema = "public", catalog = "repair_service")
public class ServiceUnitType {
    private int serviceUnitType;
    private String typeName;

    @Id
    @Column(name = "service_unit_type")
    public int getServiceUnitType() {
        return serviceUnitType;
    }

    public void setServiceUnitType(int serviceUnitType) {
        this.serviceUnitType = serviceUnitType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceUnitType that = (ServiceUnitType) o;

        if (serviceUnitType != that.serviceUnitType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceUnitType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
