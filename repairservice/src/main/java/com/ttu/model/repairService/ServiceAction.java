package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "service_action", schema = "public", catalog = "repair_service")
public class ServiceAction {
    private int serviceAction;
    private ServiceActionStatusType serviceActionStatusType;
    private ServiceType serviceType;
    private ServiceDevice serviceDevice;
    private ServiceOrder serviceOrder;
    private BigInteger serviceAmount;
    private BigInteger price;
    private Date priceUpdated;
    private String actionDescription;
    private Date created;
    private Integer createdBy;

    @Id
    @SequenceGenerator(name = "service_action_id", sequenceName = "service_action_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "service_action_id")
    @Column(name = "service_action")
    public int getServiceAction() {
        return serviceAction;
    }

    public void setServiceAction(int serviceAction) {
        this.serviceAction = serviceAction;
    }

    @ManyToOne
    @JoinColumn(name = "service_action_status_type")
    public ServiceActionStatusType getServiceActionStatusType() {
        return serviceActionStatusType;
    }

    public void setServiceActionStatusType(ServiceActionStatusType serviceActionStatusType) {
        this.serviceActionStatusType = serviceActionStatusType;
    }

    @ManyToOne
    @JoinColumn(name = "service_type")
    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    @ManyToOne
    @JoinColumn(name = "service_device")
    public ServiceDevice getServiceDevice() {
        return serviceDevice;
    }

    public void setServiceDevice(ServiceDevice serviceDevice) {
        this.serviceDevice = serviceDevice;
    }

    @ManyToOne
    @JoinColumn(name = "service_order")
    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(ServiceOrder serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    @Basic
    @Min(0)
    @Column(name = "service_amount")
    public BigInteger getServiceAmount() {
        return serviceAmount;
    }

    public void setServiceAmount(BigInteger serviceAmount) {
        this.serviceAmount = serviceAmount;
    }

    @Basic
    @Min(0)
    @Column(name = "price")
    public BigInteger getPrice() {
        return price;
    }

    public void setPrice(BigInteger price) {
        this.price = price;
    }

    @Basic
    @Column(name = "price_updated", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public Date getPriceUpdated() {
        return priceUpdated;
    }

    public void setPriceUpdated(Date priceUpdated) {
        this.priceUpdated = priceUpdated;
    }

    @Basic
    @Column(name = "action_description")
    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    @Basic
    @Column(name = "created", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Basic
    @Column(name = "created_by")
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceAction that = (ServiceAction) o;

        if (serviceAction != that.serviceAction) return false;
        if (serviceActionStatusType != null ? !serviceActionStatusType.equals(that.serviceActionStatusType) : that.serviceActionStatusType != null)
            return false;
        if (serviceType != null ? !serviceType.equals(that.serviceType) : that.serviceType != null) return false;
        if (serviceDevice != null ? !serviceDevice.equals(that.serviceDevice) : that.serviceDevice != null)
            return false;
        if (serviceOrder != null ? !serviceOrder.equals(that.serviceOrder) : that.serviceOrder != null) return false;
        if (serviceAmount != null ? !serviceAmount.equals(that.serviceAmount) : that.serviceAmount != null)
            return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (priceUpdated != null ? !priceUpdated.equals(that.priceUpdated) : that.priceUpdated != null) return false;
        if (actionDescription != null ? !actionDescription.equals(that.actionDescription) : that.actionDescription != null)
            return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceAction;
        result = 31 * result + (serviceActionStatusType != null ? serviceActionStatusType.hashCode() : 0);
        result = 31 * result + (serviceType != null ? serviceType.hashCode() : 0);
        result = 31 * result + (serviceDevice != null ? serviceDevice.hashCode() : 0);
        result = 31 * result + (serviceOrder != null ? serviceOrder.hashCode() : 0);
        result = 31 * result + (serviceAmount != null ? serviceAmount.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (priceUpdated != null ? priceUpdated.hashCode() : 0);
        result = 31 * result + (actionDescription != null ? actionDescription.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        return result;
    }
}
