package com.ttu.model.repairService;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "service_action_status_type", schema = "public", catalog = "repair_service")
public class ServiceActionStatusType {
    private int serviceActionStatusType;
    private String typeName;

    @Id
    @Column(name = "service_action_status_type")
    public int getServiceActionStatusType() {
        return serviceActionStatusType;
    }

    public void setServiceActionStatusType(int serviceActionStatusType) {
        this.serviceActionStatusType = serviceActionStatusType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceActionStatusType that = (ServiceActionStatusType) o;

        if (serviceActionStatusType != that.serviceActionStatusType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceActionStatusType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
