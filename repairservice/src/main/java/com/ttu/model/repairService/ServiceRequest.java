package com.ttu.model.repairService;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import com.ttu.model.subject.Customer;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "service_request", schema = "public", catalog = "repair_service")
public class ServiceRequest {
    private int serviceRequest;
    private Customer customer;
    private Integer createdBy;
    private Date created;
    private String serviceDescByCustomer;
    private String serviceDescByEmployee;
    private ServiceRequestStatusType serviceRequestStatusType;
    private ServiceOrder serviceOrder;

    @Id
    @SequenceGenerator(name = "service_request_id", sequenceName = "service_request_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "service_request_id")
    @Column(name = "service_request")
    public int getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(int serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    @ManyToOne
    @NotNull
    @JoinColumn(name = "customer")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Basic
    @Column(name = "created_by", updatable = false)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "created", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Basic
    @NotEmpty
    @Column(name = "service_desc_by_customer")
    public String getServiceDescByCustomer() {
        return serviceDescByCustomer;
    }

    public void setServiceDescByCustomer(String serviceDescByCustomer) {
        this.serviceDescByCustomer = serviceDescByCustomer;
    }

    @Basic
    @NotEmpty
    @Column(name = "service_desc_by_employee")
    public String getServiceDescByEmployee() {
        return serviceDescByEmployee;
    }

    public void setServiceDescByEmployee(String serviceDescByEmployee) {
        this.serviceDescByEmployee = serviceDescByEmployee;
    }

    @ManyToOne
    @NotNull
    @JoinColumn(name = "service_request_status_type")
    public ServiceRequestStatusType getServiceRequestStatusType() {
        return serviceRequestStatusType;
    }

    public void setServiceRequestStatusType(ServiceRequestStatusType serviceRequestStatusType) {
        this.serviceRequestStatusType = serviceRequestStatusType;
    }

    @OneToOne(cascade=CascadeType.ALL, mappedBy="serviceRequest", targetEntity = ServiceOrder.class)
    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(ServiceOrder serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceRequest that = (ServiceRequest) o;

        if (serviceRequest != that.serviceRequest) return false;
        if (customer != null ? !customer.equals(that.customer) : that.customer != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (serviceDescByCustomer != null ? !serviceDescByCustomer.equals(that.serviceDescByCustomer) : that.serviceDescByCustomer != null)
            return false;
        if (serviceDescByEmployee != null ? !serviceDescByEmployee.equals(that.serviceDescByEmployee) : that.serviceDescByEmployee != null)
            return false;
        if (serviceRequestStatusType != null ? !serviceRequestStatusType.equals(that.serviceRequestStatusType) : that.serviceRequestStatusType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceRequest;
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (serviceDescByCustomer != null ? serviceDescByCustomer.hashCode() : 0);
        result = 31 * result + (serviceDescByEmployee != null ? serviceDescByEmployee.hashCode() : 0);
        result = 31 * result + (serviceRequestStatusType != null ? serviceRequestStatusType.hashCode() : 0);
        return result;
    }
}
