package com.ttu.model.repairService;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "invoice_status_type", schema = "public", catalog = "repair_service")
public class InvoiceStatusType {
    private int invoiceStatusType;
    private String typeName;

    @Id
    @Column(name = "invoice_status_type")
    public int getInvoiceStatusType() {
        return invoiceStatusType;
    }

    public void setInvoiceStatusType(int invoiceStatusType) {
        this.invoiceStatusType = invoiceStatusType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvoiceStatusType that = (InvoiceStatusType) o;

        if (invoiceStatusType != that.invoiceStatusType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = invoiceStatusType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
