package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "so_status_type", schema = "public", catalog = "repair_service")
public class SoStatusType {
    private int soStatusType;
    private String typeName;

    @Id
    @Column(name = "so_status_type")
    public int getSoStatusType() {
        return soStatusType;
    }

    public void setSoStatusType(int soStatusType) {
        this.soStatusType = soStatusType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SoStatusType that = (SoStatusType) o;

        if (soStatusType != that.soStatusType) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = soStatusType;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
