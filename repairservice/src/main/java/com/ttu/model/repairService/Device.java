package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
@Table(name = "device", schema = "public", catalog = "repair_service")
public class Device {
    private int device;
    private DeviceType deviceType;
    private String name;
    private String regNo;
    private String description;
    private String model;
    private String manufacturer;

    @Id
    @SequenceGenerator(name = "device_id", sequenceName = "device_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_id")
    @Column(name = "device")
    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    @ManyToOne
    @JoinColumn(name = "device_type")
    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    @Basic
    @NotEmpty
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "reg_no")
    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "model")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Basic
    @Column(name = "manufacturer")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Device device1 = (Device) o;

        if (device != device1.device) return false;
        if (deviceType != null ? !deviceType.equals(device1.deviceType) : device1.deviceType != null) return false;
        if (name != null ? !name.equals(device1.name) : device1.name != null) return false;
        if (regNo != null ? !regNo.equals(device1.regNo) : device1.regNo != null) return false;
        if (description != null ? !description.equals(device1.description) : device1.description != null) return false;
        if (model != null ? !model.equals(device1.model) : device1.model != null) return false;
        if (manufacturer != null ? !manufacturer.equals(device1.manufacturer) : device1.manufacturer != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = device;
        result = 31 * result + (deviceType != null ? deviceType.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (regNo != null ? regNo.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        return result;
    }
}
