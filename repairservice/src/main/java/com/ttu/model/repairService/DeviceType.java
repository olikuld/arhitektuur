package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import javax.persistence.*;

@Entity
@Table(name = "device_type", schema = "public", catalog = "repair_service")
public class DeviceType {
    private int deviceType;
    private DeviceType superType;
    private Integer level;
    private String typeName;

    public DeviceType() {}
    public DeviceType(Integer level, String typeName) {
        this.level = level;
        this.typeName = typeName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_type_id")
    @SequenceGenerator(name = "device_type_id", sequenceName = "device_type_id")
    @Column(name = "device_type")
    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    @ManyToOne
    @JoinColumn(name = "super_type")
    public DeviceType getSuperType() {
        return superType;
    }

    public void setSuperType(DeviceType superType) {
        this.superType = superType;
    }

    @Basic
    @Column(name = "level")
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceType that = (DeviceType) o;

        if (deviceType != that.deviceType) return false;
        if (superType != null ? !superType.equals(that.superType) : that.superType != null) return false;
        if (level != null ? !level.equals(that.level) : that.level != null) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = deviceType;
        result = 31 * result + (superType != null ? superType.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}
