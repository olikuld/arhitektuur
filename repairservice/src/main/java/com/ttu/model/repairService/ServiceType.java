package com.ttu.model.repairService;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "service_type", schema = "public", catalog = "repair_service")
public class ServiceType {
    private int serviceType;
    private Integer serviceUnitType;
    private String typeName;
    private BigInteger servicePrice;

    @Id
    @Column(name = "service_type")
    public int getServiceType() {
        return serviceType;
    }

    public void setServiceType(int serviceType) {
        this.serviceType = serviceType;
    }

    @Basic
    @Column(name = "service_unit_type")
    public Integer getServiceUnitType() {
        return serviceUnitType;
    }

    public void setServiceUnitType(Integer serviceUnitType) {
        this.serviceUnitType = serviceUnitType;
    }

    @Basic
    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Basic
    @Column(name = "service_price")
    public BigInteger getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(BigInteger servicePrice) {
        this.servicePrice = servicePrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceType that = (ServiceType) o;

        if (serviceType != that.serviceType) return false;
        if (serviceUnitType != null ? !serviceUnitType.equals(that.serviceUnitType) : that.serviceUnitType != null)
            return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;
        if (servicePrice != null ? !servicePrice.equals(that.servicePrice) : that.servicePrice != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceType;
        result = 31 * result + (serviceUnitType != null ? serviceUnitType.hashCode() : 0);
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        result = 31 * result + (servicePrice != null ? servicePrice.hashCode() : 0);
        return result;
    }
}
