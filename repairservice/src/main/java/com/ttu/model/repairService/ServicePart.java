package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.BigInteger;
import java.sql.Timestamp;

@Entity
@Table(name = "service_part", schema = "public", catalog = "repair_service")
public class ServicePart {
    private int servicePart;
    private ServiceOrder serviceOrder;
    private ServiceDevice serviceDevice;
    private String partName;
    private String serialNo;
    private Integer partCount;
    private BigInteger partPrice;
    private Timestamp created;
    private Integer createdBy;

    @Id
    @SequenceGenerator(name = "service_part_id", sequenceName = "service_part_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "service_part_id")
    @Column(name = "service_part")
    public int getServicePart() {
        return servicePart;
    }

    public void setServicePart(int servicePart) {
        this.servicePart = servicePart;
    }

    @ManyToOne
    @JoinColumn(name = "service_order")
    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(ServiceOrder serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    @ManyToOne
    @JoinColumn(name = "service_device")
    public ServiceDevice getServiceDevice() {
        return serviceDevice;
    }

    public void setServiceDevice(ServiceDevice serviceDevice) {
        this.serviceDevice = serviceDevice;
    }

    @Basic
    @Column(name = "part_name")
    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    @Basic
    @Column(name = "serial_no")
    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    @Basic
    @Min(0)
    @Column(name = "part_count")
    public Integer getPartCount() {
        return partCount;
    }

    public void setPartCount(Integer partCount) {
        this.partCount = partCount;
    }

    @Basic
    @Min(0)
    @Column(name = "part_price")
    public BigInteger getPartPrice() {
        return partPrice;
    }

    public void setPartPrice(BigInteger partPrice) {
        this.partPrice = partPrice;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "created_by")
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServicePart that = (ServicePart) o;

        if (servicePart != that.servicePart) return false;
        if (serviceOrder != null ? !serviceOrder.equals(that.serviceOrder) : that.serviceOrder != null) return false;
        if (serviceDevice != null ? !serviceDevice.equals(that.serviceDevice) : that.serviceDevice != null)
            return false;
        if (partName != null ? !partName.equals(that.partName) : that.partName != null) return false;
        if (serialNo != null ? !serialNo.equals(that.serialNo) : that.serialNo != null) return false;
        if (partCount != null ? !partCount.equals(that.partCount) : that.partCount != null) return false;
        if (partPrice != null ? !partPrice.equals(that.partPrice) : that.partPrice != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = servicePart;
        result = 31 * result + (serviceOrder != null ? serviceOrder.hashCode() : 0);
        result = 31 * result + (serviceDevice != null ? serviceDevice.hashCode() : 0);
        result = 31 * result + (partName != null ? partName.hashCode() : 0);
        result = 31 * result + (serialNo != null ? serialNo.hashCode() : 0);
        result = 31 * result + (partCount != null ? partCount.hashCode() : 0);
        result = 31 * result + (partPrice != null ? partPrice.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        return result;
    }
}
