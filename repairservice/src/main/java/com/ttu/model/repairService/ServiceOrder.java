package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

@Entity
@Table(name = "service_order", schema = "public", catalog = "repair_service")
public class ServiceOrder {
    private int serviceOrder;
    private SoStatusType soStatusType;
    private Integer createdBy;
    private ServiceRequest serviceRequest;
    private Integer updatedBy;
    private Date created;
    private Date updated;
    private Date statusChanged;
    private Integer statusChangedBy;
    private BigInteger priceTotal;
    private String note;
    private List<ServiceDevice> serviceDevices = new ArrayList<>();
    private List<ServicePart> serviceParts = new ArrayList<>();
    private List<ServiceAction> serviceActions = new ArrayList<>();

    @Id
    @SequenceGenerator(name = "service_order_id", sequenceName = "service_order_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "service_order_id")
    @Column(name = "service_order")
    public int getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(int serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    @ManyToOne
    @NotNull
    @JoinColumn(name = "so_status_type")
    public SoStatusType getSoStatusType() {
        return soStatusType;
    }

    public void setSoStatusType(SoStatusType soStatusType) {
        this.soStatusType = soStatusType;
    }

    @Basic
    @Column(name = "created_by")
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @OneToOne
    @JoinColumn(name = "service_request")
    public ServiceRequest getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(ServiceRequest serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    @Basic
    @Column(name = "updated_by")
    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "created", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "status_changed", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public Date getStatusChanged() {
        return statusChanged;
    }

    public void setStatusChanged(Date statusChanged) {
        this.statusChanged = statusChanged;
    }

    @Basic
    @Column(name = "status_changed_by")
    public Integer getStatusChangedBy() {
        return statusChangedBy;
    }

    public void setStatusChangedBy(Integer statusChangedBy) {
        this.statusChangedBy = statusChangedBy;
    }

    @Basic
    @Column(name = "price_total")
    public BigInteger getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(BigInteger priceTotal) {
        this.priceTotal = priceTotal;
    }

    @Basic
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Valid
    @OrderBy("serviceDevice")
    @OneToMany(targetEntity = ServiceDevice.class, cascade = CascadeType.ALL, mappedBy = "serviceOrder")
    public List<ServiceDevice> getServiceDevices() {
        return serviceDevices;
    }

    public void setServiceDevices(List<ServiceDevice> serviceDevices) {
        this.serviceDevices = serviceDevices;
    }

    @Valid
    @OrderBy("servicePart")
    @OneToMany(targetEntity = ServicePart.class, cascade = CascadeType.ALL, mappedBy = "serviceOrder")
    public List<ServicePart> getServiceParts() {
        return serviceParts;
    }

    public void setServiceParts(List<ServicePart> serviceParts) {
        this.serviceParts = serviceParts;
    }

    @Valid
    @OrderBy("serviceAction")
    @OneToMany(targetEntity = ServiceAction.class, cascade = CascadeType.ALL, mappedBy = "serviceOrder")
    public List<ServiceAction> getServiceActions() {
        return serviceActions;
    }

    public void setServiceActions(List<ServiceAction> serviceActions) {
        this.serviceActions = serviceActions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceOrder that = (ServiceOrder) o;

        if (serviceOrder != that.serviceOrder) return false;
        if (soStatusType != null ? !soStatusType.equals(that.soStatusType) : that.soStatusType != null)
            return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (serviceRequest != null ? !serviceRequest.equals(that.serviceRequest) : that.serviceRequest != null)
            return false;
        if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;
        if (statusChanged != null ? !statusChanged.equals(that.statusChanged) : that.statusChanged != null)
            return false;
        if (statusChangedBy != null ? !statusChangedBy.equals(that.statusChangedBy) : that.statusChangedBy != null)
            return false;
        if (priceTotal != null ? !priceTotal.equals(that.priceTotal) : that.priceTotal != null) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceOrder;
        result = 31 * result + (soStatusType != null ? soStatusType.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (serviceRequest != null ? serviceRequest.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (statusChanged != null ? statusChanged.hashCode() : 0);
        result = 31 * result + (statusChangedBy != null ? statusChangedBy.hashCode() : 0);
        result = 31 * result + (priceTotal != null ? priceTotal.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }
}
