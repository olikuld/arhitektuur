package com.ttu.model.repairService;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "service_device", schema = "public", catalog = "repair_service")
public class ServiceDevice {
    private int serviceDevice;
    private ServiceDeviceStatusType serviceDeviceStatusType;
    private Device device;
    private ServiceOrder serviceOrder;
    private Timestamp toStore;
    private Timestamp fromStore;
    private String serviceDescription;
    private Date statusChanged;
    private BigInteger storeStatus;
    private List<ServiceNote> serviceNotes = new ArrayList<>();
    private List<ServiceAction> serviceActions = new ArrayList<>();
    private List<ServicePart> serviceParts = new ArrayList<>();

    @Id
    @SequenceGenerator(name = "service_device_id", sequenceName = "service_device_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "service_device_id")
    @Column(name = "service_device")
    public int getServiceDevice() {
        return serviceDevice;
    }

    public void setServiceDevice(int serviceDevice) {
        this.serviceDevice = serviceDevice;
    }

    @ManyToOne
    @JoinColumn(name = "service_device_status_type")
    public ServiceDeviceStatusType getServiceDeviceStatusType() {
        return serviceDeviceStatusType;
    }

    public void setServiceDeviceStatusType(ServiceDeviceStatusType serviceDeviceStatusType) {
        this.serviceDeviceStatusType = serviceDeviceStatusType;
    }

    @ManyToOne
    @JoinColumn(name = "device")
    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @ManyToOne
    @JoinColumn(name = "service_order")
    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(ServiceOrder serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    @Basic
    @Column(name = "to_store")
    public Timestamp getToStore() {
        return toStore;
    }

    public void setToStore(Timestamp toStore) {
        this.toStore = toStore;
    }

    @Basic
    @Column(name = "from_store")
    public Timestamp getFromStore() {
        return fromStore;
    }

    public void setFromStore(Timestamp fromStore) {
        this.fromStore = fromStore;
    }

    @Basic
    @Column(name = "service_description")
    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    @Basic
    @Column(name = "status_changed", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public Date getStatusChanged() {
        return statusChanged;
    }

    public void setStatusChanged(Date statusChanged) {
        this.statusChanged = statusChanged;
    }

    @Basic
    @Column(name = "store_status")
    public BigInteger getStoreStatus() {
        return storeStatus;
    }

    public void setStoreStatus(BigInteger storeStatus) {
        this.storeStatus = storeStatus;
    }

    @Valid
    @OrderBy("serviceNote")
    @OneToMany(targetEntity = ServiceNote.class, cascade = CascadeType.REMOVE, mappedBy = "serviceDevice")
    public List<ServiceNote> getServiceNotes() {
        return serviceNotes;
    }

    public void setServiceNotes(List<ServiceNote> serviceNotes) {
        this.serviceNotes = serviceNotes;
    }

    @Valid
    @OrderBy("serviceAction")
    @OneToMany(targetEntity = ServiceAction.class, cascade = CascadeType.REMOVE, mappedBy = "serviceDevice")
    public List<ServiceAction> getServiceActions() {
        return serviceActions;
    }

    public void setServiceActions(List<ServiceAction> serviceActions) {
        this.serviceActions = serviceActions;
    }

    @Valid
    @OrderBy("servicePart")
    @OneToMany(targetEntity = ServicePart.class, cascade = CascadeType.REMOVE, mappedBy = "serviceDevice")
    public List<ServicePart> getServiceParts() {
        return serviceParts;
    }

    public void setServiceParts(List<ServicePart> serviceParts) {
        this.serviceParts = serviceParts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceDevice that = (ServiceDevice) o;

        if (serviceDevice != that.serviceDevice) return false;
        if (serviceDeviceStatusType != null ? !serviceDeviceStatusType.equals(that.serviceDeviceStatusType) : that.serviceDeviceStatusType != null)
            return false;
        if (device != null ? !device.equals(that.device) : that.device != null) return false;
        if (serviceOrder != null ? !serviceOrder.equals(that.serviceOrder) : that.serviceOrder != null) return false;
        if (toStore != null ? !toStore.equals(that.toStore) : that.toStore != null) return false;
        if (fromStore != null ? !fromStore.equals(that.fromStore) : that.fromStore != null) return false;
        if (serviceDescription != null ? !serviceDescription.equals(that.serviceDescription) : that.serviceDescription != null)
            return false;
        if (statusChanged != null ? !statusChanged.equals(that.statusChanged) : that.statusChanged != null)
            return false;
        if (storeStatus != null ? !storeStatus.equals(that.storeStatus) : that.storeStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceDevice;
        result = 31 * result + (serviceDeviceStatusType != null ? serviceDeviceStatusType.hashCode() : 0);
        result = 31 * result + (device != null ? device.hashCode() : 0);
        result = 31 * result + (serviceOrder != null ? serviceOrder.hashCode() : 0);
        result = 31 * result + (toStore != null ? toStore.hashCode() : 0);
        result = 31 * result + (fromStore != null ? fromStore.hashCode() : 0);
        result = 31 * result + (serviceDescription != null ? serviceDescription.hashCode() : 0);
        result = 31 * result + (statusChanged != null ? statusChanged.hashCode() : 0);
        result = 31 * result + (storeStatus != null ? storeStatus.hashCode() : 0);
        return result;
    }
}
