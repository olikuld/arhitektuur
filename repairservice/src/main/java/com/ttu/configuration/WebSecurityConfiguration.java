package com.ttu.configuration;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.subject.UserAccount;
import com.ttu.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
public class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

    public static final int SUBJECT_TYPE_EMPLOYEE = 3;

    @Autowired
    UserAccountRepository userAccountRepository;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(new Md5PasswordEncoder());
    }

    @Bean
    UserDetailsService userDetailsService() {
        return new UserDetailsService() {

            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                UserAccount user = userAccountRepository.findByUsername(username);

                if (user != null) {
                    if (user.getSubjectType() == SUBJECT_TYPE_EMPLOYEE) {
                        return new User(user.getUsername(), user.getPassword(), true, true, true, true, AuthorityUtils.createAuthorityList("ADMIN"));
                    }

                    return new User(username, user.getPassword(), true, true, true, true, AuthorityUtils.createAuthorityList("USER"));
                }
                else {
                    throw new UsernameNotFoundException("Could not find user '"  + username + "'");
                }
            }

        };
    }
}
