package com.ttu.repository;

import com.ttu.model.repairService.ServiceDeviceStatusType;
import org.springframework.data.repository.CrudRepository;

public interface ServiceDeviceStatusTypeRepository extends CrudRepository<ServiceDeviceStatusType, Integer> {}
