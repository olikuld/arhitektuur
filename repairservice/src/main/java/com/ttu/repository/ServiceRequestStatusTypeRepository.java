package com.ttu.repository;

import com.ttu.model.repairService.ServiceRequestStatusType;
import org.springframework.data.repository.CrudRepository;

public interface ServiceRequestStatusTypeRepository extends CrudRepository<ServiceRequestStatusType, Integer> {}