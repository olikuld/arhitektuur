package com.ttu.repository;

import com.ttu.model.repairService.Device;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeviceRepository extends CrudRepository<Device, Integer> {

    public List<Device> findByNameContainingIgnoreCase(String name);

}
