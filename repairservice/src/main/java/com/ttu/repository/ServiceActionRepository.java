package com.ttu.repository;

import com.ttu.model.repairService.ServiceAction;
import com.ttu.model.repairService.ServiceOrder;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface ServiceActionRepository extends CrudRepository<ServiceAction, Integer> {

    ArrayList<ServiceAction> findByServiceOrder(ServiceOrder serviceOrder);

}
