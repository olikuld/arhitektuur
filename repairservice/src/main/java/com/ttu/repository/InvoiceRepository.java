package com.ttu.repository;

import com.ttu.model.repairService.Invoice;
import com.ttu.model.repairService.ServiceOrder;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceRepository extends CrudRepository<Invoice, Integer> {

    public Invoice findByServiceOrder(ServiceOrder serviceOrder);

}