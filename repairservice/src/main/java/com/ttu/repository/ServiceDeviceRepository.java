package com.ttu.repository;

import com.ttu.model.repairService.ServiceDevice;
import com.ttu.model.repairService.ServiceOrder;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface ServiceDeviceRepository extends CrudRepository<ServiceDevice, Integer> {

    ArrayList<ServiceDevice> findByServiceOrder(ServiceOrder serviceOrder);

}
