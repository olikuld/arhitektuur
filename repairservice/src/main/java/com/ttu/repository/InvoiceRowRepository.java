package com.ttu.repository;

import com.ttu.model.repairService.InvoiceRow;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceRowRepository extends CrudRepository<InvoiceRow, Integer> {}