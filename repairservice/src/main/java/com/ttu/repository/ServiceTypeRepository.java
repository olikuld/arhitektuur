package com.ttu.repository;

import com.ttu.model.repairService.ServiceType;
import org.springframework.data.repository.CrudRepository;

public interface ServiceTypeRepository extends CrudRepository<ServiceType, Integer> {}
