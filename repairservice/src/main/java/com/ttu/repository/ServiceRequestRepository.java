package com.ttu.repository;

import com.ttu.model.repairService.ServiceRequest;
import com.ttu.model.subject.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServiceRequestRepository extends CrudRepository<ServiceRequest, Integer> {

    public List<ServiceRequest> findByCustomer(Customer customer);

}
