package com.ttu.repository;

import com.ttu.model.repairService.ServiceActionStatusType;
import org.springframework.data.repository.CrudRepository;

public interface ServiceActionStatusTypeRepository extends CrudRepository<ServiceActionStatusType, Integer> {}
