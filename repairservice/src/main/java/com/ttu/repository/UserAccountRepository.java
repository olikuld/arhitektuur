package com.ttu.repository;

import com.ttu.model.subject.UserAccount;
import org.springframework.data.repository.CrudRepository;

public interface UserAccountRepository extends CrudRepository<UserAccount, Integer> {

    public UserAccount findByUsername(String username);

}