package com.ttu.repository;

import com.ttu.model.repairService.ServiceOrder;
import com.ttu.model.repairService.ServiceRequest;
import org.springframework.data.repository.CrudRepository;

public interface ServiceOrderRepository extends CrudRepository<ServiceOrder, Integer> {

    ServiceOrder findByServiceRequest(ServiceRequest serviceRequest);

}
