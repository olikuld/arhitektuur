package com.ttu.repository;

import com.ttu.model.repairService.InvoiceStatusType;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceStatusTypeRepository extends CrudRepository<InvoiceStatusType, Integer> {}