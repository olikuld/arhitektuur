package com.ttu.repository;

import com.ttu.model.repairService.ServiceOrder;
import com.ttu.model.repairService.ServicePart;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface ServicePartRepository extends CrudRepository<ServicePart, Integer> {

    ArrayList<ServicePart> findByServiceOrder(ServiceOrder serviceOrder);

}
