package com.ttu.repository;

import com.ttu.model.repairService.DeviceType;
import org.springframework.data.repository.CrudRepository;

public interface DeviceTypeRepository extends CrudRepository<DeviceType, Integer> {}
