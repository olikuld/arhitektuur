package com.ttu.repository;

import com.ttu.model.subject.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Integer> {

    public List<Person> findByFirstNameContainingIgnoreCase(String firstName);
    public List<Person> findByLastNameContainingIgnoreCase(String lastName);
    public List<Person> findByFirstNameAndLastNameContainingAllIgnoreCase(String firstName, String lastName);

}