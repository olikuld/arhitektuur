package com.ttu.repository;

import com.ttu.model.subject.Customer;
import com.ttu.model.subject.Person;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    public Customer findBySubject(Person subject);

}