package com.ttu.repository;

import com.ttu.model.repairService.SoStatusType;
import org.springframework.data.repository.CrudRepository;

public interface ServiceOrderStatusRepository extends CrudRepository<SoStatusType, Integer> {}
