package com.ttu.controller.customer;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import com.ttu.model.repairService.ServiceOrder;
import com.ttu.model.repairService.ServiceRequest;
import com.ttu.model.subject.Customer;
import com.ttu.model.subject.UserAccount;
import com.ttu.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@EnableAutoConfiguration
@RequestMapping("/customer")
public class CustomerServiceOrderController {

    @Autowired
    private ServiceOrderRepository serviceOrderRepository;

    @Autowired
    private ServiceRequestRepository serviceRequestRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String serviceOrders(Model model, Principal principal) {
        UserAccount user = userAccountRepository.findByUsername(principal.getName());
        Customer customer = customerRepository.findBySubject(user.getSubject());

        List<ServiceRequest> serviceRequests = customer.getServiceRequests();
        List<ServiceOrder> serviceOrders = new ArrayList<>();

        for (ServiceRequest serviceRequest : serviceRequests) {
            if (serviceRequest.getServiceOrder() != null && serviceRequest.getServiceOrder().getServiceOrder() > 0) {
                serviceOrders.add(serviceRequest.getServiceOrder());
            }
        }

        model.addAttribute("orders", serviceOrders);
        return "customer/orders";
    }


}