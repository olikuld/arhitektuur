package com.ttu.controller.employee;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.repairService.*;
import com.ttu.model.subject.UserAccount;
import com.ttu.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;

@Controller
@EnableAutoConfiguration
@RequestMapping("/request")
public class ServiceRequestController {

    @Autowired
    private ServiceRequestRepository serviceRequestRepository;

    @Autowired
    private ServiceRequestStatusTypeRepository serviceRequestStatusTypeRepository;

    @Autowired
    private ServiceOrderRepository serviceOrderRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @InitBinder
    public void dataBinder(WebDataBinder binder) {
        binder.setDisallowedFields("createdAt", "createdBy");
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String serviceRequests(Model model, Principal principal) {
        model.addAttribute("requests", serviceRequestRepository.findAll());
        return "requests";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newServiceRequest(Model model, Principal principal) {
        ServiceRequest serviceRequest = new ServiceRequest();
        model.addAttribute("serviceRequest", serviceRequest);
        model.addAttribute("serviceRequestStatusTypes", serviceRequestStatusTypeRepository.findAll());

        return "new-request";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String saveNewServiceRequest(
            @Valid @ModelAttribute("serviceRequest") ServiceRequest serviceRequest,
            BindingResult bindingResult,
            Model model,
            Principal principal) {

        UserAccount user = userAccountRepository.findByUsername(principal.getName());

        serviceRequest.setCreatedBy(user.getSubject().getPerson());
        serviceRequest.setCreated(new Date());

        if (bindingResult.hasErrors()) {
            model.addAttribute("serviceRequest", serviceRequest);
            model.addAttribute("serviceRequestStatusTypes", serviceRequestStatusTypeRepository.findAll());
            return "new-request";
        }

        serviceRequestRepository.save(serviceRequest);

        return "redirect:/request/" + serviceRequest.getServiceRequest();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String serviceRequest(@PathVariable Integer id, Model model, Principal principal) {
        ServiceRequest serviceRequest = serviceRequestRepository.findOne(id);

        model.addAttribute("serviceRequest", serviceRequest);
        model.addAttribute("serviceRequestStatusTypes", serviceRequestStatusTypeRepository.findAll());

        ServiceOrder serviceOrder = serviceOrderRepository.findByServiceRequest(serviceRequest);
        if (serviceOrder != null) {
            model.addAttribute(serviceOrder);
        }

        return "request";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public String saveServiceRequest(
            @PathVariable Integer id,
            @Valid @ModelAttribute("serviceRequest") ServiceRequest serviceRequest,
            BindingResult bindingResult,
            Model model,
            Principal principal) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("serviceRequest", serviceRequest);
            model.addAttribute("serviceRequestStatusTypes", serviceRequestStatusTypeRepository.findAll());
            return "request";
        }

        serviceRequestRepository.save(serviceRequest);

        return "redirect:/request/" + serviceRequest.getServiceRequest();
    }
}