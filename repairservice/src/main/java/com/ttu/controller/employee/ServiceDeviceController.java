package com.ttu.controller.employee;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.repairService.ServiceDevice;
import com.ttu.repository.ServiceDeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ServiceDeviceController {

    @Autowired
    ServiceDeviceRepository serviceDeviceRepository;

    @RequestMapping(value = "/service-device/{id}/delete", method = RequestMethod.GET)
    public String deleteServiceDevice(@PathVariable Integer id) {
        ServiceDevice serviceDevice = serviceDeviceRepository.findOne(id);

        serviceDeviceRepository.delete(id);

        return "redirect:/order/" + serviceDevice.getServiceOrder().getServiceOrder();
    }

}
