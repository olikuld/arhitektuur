package com.ttu.controller.employee;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import com.ttu.model.subject.Customer;
import com.ttu.model.subject.Person;
import com.ttu.repository.CustomerRepository;
import com.ttu.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@EnableAutoConfiguration
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    CustomerRepository customerRepository;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String showCustomerSearchForm(Model model) {
        model.addAttribute("person", new Person());
        return "search/customer";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String showCustomerSearchResults(Model model, Person person) {
        List<Person> results = new ArrayList<>();
        List<Customer> customers = new ArrayList<>();

        if (person.getFirstName().length() > 0 && person.getLastName().length() > 0) {
            results = personRepository.findByFirstNameAndLastNameContainingAllIgnoreCase(person.getFirstName(), person.getLastName());
        }
        else if (person.getFirstName().length() > 0) {
            results = personRepository.findByFirstNameContainingIgnoreCase(person.getFirstName());
        }
        else if (person.getLastName().length() > 0) {
            results = personRepository.findByLastNameContainingIgnoreCase(person.getLastName());
        }

        for (Person result : results) {
            Customer customer = customerRepository.findBySubject(result);
            if (customer != null) {
                customers.add(customer);
            }
        }

        model.addAttribute("customers", customers);

        return "search/customer";
    }
}