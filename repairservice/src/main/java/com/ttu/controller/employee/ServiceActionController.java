package com.ttu.controller.employee;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.repairService.ServiceAction;
import com.ttu.repository.ServiceActionRepository;
import com.ttu.repository.ServiceOrderRepository;
import com.ttu.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ServiceActionController {

    @Autowired
    ServiceActionRepository serviceActionRepository;

    @RequestMapping(value = "/service-action/{id}/delete", method = RequestMethod.GET)
    public String deleteServiceAction(@PathVariable Integer id) {
        ServiceAction serviceAction = serviceActionRepository.findOne(id);

        serviceActionRepository.delete(id);

        return "redirect:/order/" + serviceAction.getServiceOrder().getServiceOrder();
    }

}
