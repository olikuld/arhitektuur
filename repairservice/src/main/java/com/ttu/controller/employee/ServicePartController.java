package com.ttu.controller.employee;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import com.ttu.model.repairService.ServicePart;
import com.ttu.repository.ServiceOrderRepository;
import com.ttu.repository.ServicePartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class ServicePartController {

    @Autowired
    ServicePartRepository servicePartRepository;

    @RequestMapping(value = "/service-part/{id}/delete", method = RequestMethod.GET)
    public String deleteServiceDevice(@PathVariable Integer id) {
        ServicePart servicePart = servicePartRepository.findOne(id);

        servicePartRepository.delete(id);

        return "redirect:/order/" + servicePart.getServiceOrder().getServiceOrder();
    }

}
