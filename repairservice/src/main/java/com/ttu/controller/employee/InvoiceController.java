package com.ttu.controller.employee;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.repairService.*;
import com.ttu.model.subject.Customer;
import com.ttu.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Controller
@EnableAutoConfiguration
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    ServiceOrderRepository serviceOrderRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    InvoiceRowRepository invoiceRowRepository;

    @Autowired
    InvoiceStatusTypeRepository invoiceStatusTypeRepository;

    @Autowired
    ServiceOrderStatusRepository serviceOrderStatusRepository;

    @RequestMapping(value = "/new/{id}", method = RequestMethod.GET)
    public String showNewInvoiceForm(@PathVariable Integer id, Model model) {
        ServiceOrder serviceOrder = serviceOrderRepository.findOne(id);

        if (invoiceRepository.findByServiceOrder(serviceOrder) != null) {
            return "redirect:/invoice/" + serviceOrder.getServiceOrder();
        }

        Invoice invoice = new Invoice();
        invoice.setServiceOrder(serviceOrder);

        model.addAttribute("invoice", invoice);
        model.addAttribute("serviceOrder", serviceOrder);
        model.addAttribute("serviceParts", serviceOrder.getServiceParts());
        model.addAttribute("serviceActions", serviceOrder.getServiceActions());
        model.addAttribute("serviceDevices", serviceOrder.getServiceDevices());

        return "new-invoice";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String saveNewInvoice(
            @Valid @ModelAttribute("invoice") Invoice invoice,
            BindingResult bindingResult,
            Model model) {

        ServiceOrder serviceOrder = invoice.getServiceOrder();

        serviceOrder.setSoStatusType(serviceOrderStatusRepository.findOne(4));

        if (bindingResult.hasErrors()) {
            model.addAttribute("invoice", invoice);
            model.addAttribute("serviceOrder", serviceOrder);
            model.addAttribute("serviceParts", serviceOrder.getServiceParts());
            model.addAttribute("serviceActions", serviceOrder.getServiceActions());
            model.addAttribute("serviceDevices", serviceOrder.getServiceDevices());

            return "new-invoice";
        }

        Customer customer = serviceOrder.getServiceRequest().getCustomer();

        invoice.setCustomer(customer);
        invoice.setInvoiceStatusType(invoiceStatusTypeRepository.findOne(1));
        invoice.setInvoiceDate(new java.sql.Date(new Date().getTime()));
        invoice.setPriceTotal(serviceOrder.getPriceTotal());
        invoice.setReceiverName(customer.getSubject().getFirstName() + ' ' + customer.getSubject().getLastName());

        invoiceRepository.save(invoice);

        List<ServicePart> serviceParts = serviceOrder.getServiceParts();
        for (ServicePart servicePart : serviceParts) {
            InvoiceRow invoiceRow = new InvoiceRow();
            invoiceRow.setInvoice(invoice);
            invoiceRow.setServicePart(servicePart);
            invoiceRow.setAmount(BigInteger.valueOf(servicePart.getPartCount()));
            invoiceRow.setPriceTotal(BigInteger.valueOf(servicePart.getPartCount()).multiply(servicePart.getPartPrice()));
            invoiceRow.setInvoiceRowType(BigInteger.valueOf(1));
            invoiceRowRepository.save(invoiceRow);
        }

        List<ServiceAction> serviceActions = serviceOrder.getServiceActions();
        for (ServiceAction serviceAction : serviceActions) {
            InvoiceRow invoiceRow = new InvoiceRow();
            invoiceRow.setInvoice(invoice);
            invoiceRow.setServiceAction(serviceAction);
            invoiceRow.setActionPartDescription(serviceAction.getActionDescription());
            invoiceRow.setAmount(serviceAction.getServiceAmount());
            invoiceRow.setPriceTotal(serviceAction.getServiceAmount().multiply(serviceAction.getPrice()));
            invoiceRow.setInvoiceRowType(BigInteger.valueOf(2));
            invoiceRowRepository.save(invoiceRow);
        }

        return "redirect:/invoice/" + serviceOrder.getServiceOrder();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showInvoice(@PathVariable Integer id, Model model) {
        ServiceOrder serviceOrder = serviceOrderRepository.findOne(id);

        Invoice invoice = invoiceRepository.findByServiceOrder(serviceOrder);

        model.addAttribute("invoice", invoice);
        model.addAttribute("serviceOrder", serviceOrder);
        model.addAttribute("serviceParts", serviceOrder.getServiceParts());
        model.addAttribute("serviceActions", serviceOrder.getServiceActions());
        model.addAttribute("serviceDevices", serviceOrder.getServiceDevices());

        return "invoice";
    }

}