package com.ttu.controller.employee;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.dao.DeviceTypeDAO;
import com.ttu.model.repairService.Device;
import com.ttu.repository.DeviceRepository;
import com.ttu.repository.DeviceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@EnableAutoConfiguration
@RequestMapping("/device")
public class DeviceController {

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    DeviceTypeRepository deviceTypeRepository;

    @Autowired
    DeviceTypeDAO deviceTypeDAO;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String showAllDevices(Model model) {
        model.addAttribute("devices", deviceRepository.findAll());
        return "devices";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String showNewDeviceForm(Model model) {
        model.addAttribute("device", new Device());
        model.addAttribute("deviceTypes", deviceTypeRepository.findAll());
        return "new-device";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String saveNewDevice(
            @Valid @ModelAttribute("device") Device device,
            BindingResult bindingResult,
            Model model,
            Principal principal) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("device", device);
            model.addAttribute("deviceTypes", deviceTypeRepository.findAll());
            return "new-device";
        }

        deviceRepository.save(device);

        return "redirect:/device/" + device.getDevice();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showDeviceForm(@PathVariable Integer id, Model model) {
        model.addAttribute("device", deviceRepository.findOne(id));
        model.addAttribute("deviceTypes", deviceTypeRepository.findAll());
        return "device";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public String saveDevice(
            @Valid @ModelAttribute("device") Device device,
            BindingResult bindingResult,
            Model model,
            Principal principal) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("device", device);
            model.addAttribute("deviceTypes", deviceTypeRepository.findAll());
            return "device";
        }

        deviceRepository.save(device);
        model.addAttribute("device", device);
        model.addAttribute("deviceTypes", deviceTypeRepository.findAll());

        return "device";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String showCustomerSearchForm(Model model) {
        model.addAttribute("device", new Device());
        return "search/device";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String showDeviceSearchResults(Model model, Device device) {
        List<Device> results = new ArrayList<>();
        model.addAttribute("results", deviceRepository.findByNameContainingIgnoreCase(device.getName()));
        return "search/device";
    }
}