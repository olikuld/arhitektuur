package com.ttu.controller.employee;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.repairService.*;
import com.ttu.model.subject.UserAccount;
import com.ttu.repository.*;
import com.ttu.validator.ServiceOrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Controller
@EnableAutoConfiguration
@RequestMapping("/order")
public class ServiceOrderController {

    @Autowired
    private ServiceOrderRepository serviceOrderRepository;

    @Autowired
    private ServiceOrderStatusRepository serviceOrderStatusRepository;

    @Autowired
    private ServiceRequestRepository serviceRequestRepository;

    @Autowired
    private ServicePartRepository servicePartRepository;

    @Autowired
    private ServiceActionRepository serviceActionRepository;

    @Autowired
    private ServiceDeviceRepository serviceDeviceRepository;

    @Autowired
    private ServiceTypeRepository serviceTypeRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private ServiceDeviceStatusTypeRepository serviceDeviceStatusTypeRepository;

    @Autowired
    private ServiceActionStatusTypeRepository serviceActionStatusTypeRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String serviceOrders(Model model, Principal principal) {
        model.addAttribute("orders", serviceOrderRepository.findAll());
        return "orders";
    }

    @RequestMapping(value = "/new/{id}", method = RequestMethod.GET)
    public String newServiceOrder(@PathVariable Integer id, Model model, Principal principal) {
        ServiceOrder serviceOrder = new ServiceOrder();
        ServiceRequest serviceRequest = serviceRequestRepository.findOne(id);

        serviceOrder.setServiceRequest(serviceRequest);
        serviceOrder.setPriceTotal(BigInteger.valueOf(0));

        addGenericData(model);
        model.addAttribute("serviceOrder", serviceOrder);
        model.addAttribute("serviceRequest", serviceRequest);

        return "new-order";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String saveNewServiceOrder(
            @Valid @ModelAttribute("serviceOrder") ServiceOrder serviceOrder,
            BindingResult bindingResult,
            @ModelAttribute("serviceRequest") ServiceRequest serviceRequest,
            Model model,
            Principal principal) {

        if (bindingResult.hasErrors()) {
            addGenericData(model);
            model.addAttribute("serviceOrder", serviceOrder);
            model.addAttribute("serviceRequest", serviceRequest);
            return "new-order";
        }

        UserAccount user = userAccountRepository.findByUsername(principal.getName());

        serviceOrder.setCreatedBy(user.getSubject().getPerson());
        serviceOrder.setCreated(new Date());

        serviceOrderRepository.save(serviceOrder);

        return "redirect:/order/" + serviceOrder.getServiceOrder();
    }

    private void addGenericData(Model model) {
        model.addAttribute("serviceDeviceStatusTypes", serviceDeviceStatusTypeRepository.findAll());
        model.addAttribute("serviceActionStatusTypes", serviceActionStatusTypeRepository.findAll());
        model.addAttribute("serviceTypes", serviceTypeRepository.findAll());
        model.addAttribute("serviceOrderStatusTypes", serviceOrderStatusRepository.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String serviceOrder(
            @PathVariable Integer id,
            Model model,
            Principal principal) {

        ServiceRequest serviceRequest = serviceRequestRepository.findOne(id);
        ServiceOrder serviceOrder = serviceOrderRepository.findByServiceRequest(serviceRequest);
        BigInteger totalPrice = BigInteger.valueOf(0);

        List<ServicePart> serviceParts = serviceOrder.getServiceParts();
        for (ServicePart servicePart : serviceParts) {
            totalPrice = totalPrice.add(servicePart.getPartPrice().multiply(BigInteger.valueOf(servicePart.getPartCount())));
        }

        List<ServiceAction> serviceActions = serviceOrder.getServiceActions();
        for (ServiceAction serviceAction : serviceActions) {
            totalPrice = totalPrice.add(serviceAction.getPrice().multiply(serviceAction.getServiceAmount()));
        }

        serviceOrder.setPriceTotal(totalPrice);
        boolean finished = serviceOrder.getSoStatusType().getSoStatusType() != 1;

        addGenericData(model);
        model.addAttribute("serviceOrder", serviceOrder);
        model.addAttribute("serviceRequest", serviceRequest);
        model.addAttribute("finished", finished);
        model.addAttribute("serviceDevices", serviceOrder.getServiceDevices());
        model.addAttribute("serviceParts", serviceParts);
        model.addAttribute("serviceActions", serviceActions);

        return "order";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public String saveServiceOrder(
            @PathVariable Integer id,
            @Valid @ModelAttribute("serviceOrder") ServiceOrder updatedServiceOrder,
            BindingResult bindingResult,
            Model model,
            Principal principal) {

        ServiceOrderValidator serviceOrderValidator = new ServiceOrderValidator();
        serviceOrderValidator.validate(updatedServiceOrder, bindingResult);

        ServiceOrder serviceOrder = serviceOrderRepository.findOne(id);

        if (bindingResult.hasErrors()) {
            addGenericData(model);
            boolean finished = serviceOrder.getSoStatusType().getSoStatusType() != 1;
            model.addAttribute("serviceOrder", updatedServiceOrder);
            model.addAttribute("finished", finished);
            model.addAttribute("serviceRequest", updatedServiceOrder.getServiceRequest());
            model.addAttribute("serviceParts", updatedServiceOrder.getServiceParts());
            model.addAttribute("serviceDevices", updatedServiceOrder.getServiceDevices());
            model.addAttribute("serviceActions", updatedServiceOrder.getServiceActions());

            return "order";
        }
        else {
            serviceOrder.setSoStatusType(updatedServiceOrder.getSoStatusType());

            List<ServicePart> updatedServiceParts = updatedServiceOrder.getServiceParts();

            for (int i = 0; i < updatedServiceParts.size(); i++) {
                ServicePart updatedServicePart = updatedServiceParts.get(i);
                ServicePart servicePart = serviceOrder.getServiceParts().get(i);

                servicePart.setServiceDevice(updatedServicePart.getServiceDevice());
                servicePart.setPartName(updatedServicePart.getPartName());
                servicePart.setSerialNo(updatedServicePart.getSerialNo());
                servicePart.setPartCount(updatedServicePart.getPartCount());
                servicePart.setPartPrice(updatedServicePart.getPartPrice());
            }

            List<ServiceDevice> updatedServiceDevices = updatedServiceOrder.getServiceDevices();

            for (int i = 0; i < updatedServiceDevices.size(); i++) {
                ServiceDevice updatedServiceDevice = updatedServiceDevices.get(i);
                ServiceDevice serviceDevice = serviceOrder.getServiceDevices().get(i);

                if (!Objects.equals(serviceDevice.getServiceDeviceStatusType(), updatedServiceDevice.getServiceDeviceStatusType())) {
                    serviceDevice.setServiceDeviceStatusType(updatedServiceDevice.getServiceDeviceStatusType());
                    serviceDevice.setStatusChanged(new Date());
                }
            }

            List<ServiceAction> updatedServiceActions = updatedServiceOrder.getServiceActions();

            for (int i = 0; i < updatedServiceActions.size(); i++) {
                ServiceAction updatedServiceAction = updatedServiceActions.get(i);
                ServiceAction serviceAction = serviceOrder.getServiceActions().get(i);

                serviceAction.setPrice(updatedServiceAction.getPrice());
                serviceAction.setActionDescription(updatedServiceAction.getActionDescription());
                serviceAction.setServiceType(updatedServiceAction.getServiceType());
                serviceAction.setServiceAmount(updatedServiceAction.getServiceAmount());
                serviceAction.setServiceDevice(updatedServiceAction.getServiceDevice());
            }

            serviceOrderRepository.save(serviceOrder);

            return "redirect:/order/" + serviceOrder.getServiceOrder();
        }
    }

    @RequestMapping(value = "/{id}/add-device/{deviceId}", method = RequestMethod.GET)
    public String addServiceDevice(@PathVariable Integer id, @PathVariable Integer deviceId) {
        ServiceOrder serviceOrder = serviceOrderRepository.findOne(id);

        ServiceDevice serviceDevice = new ServiceDevice();
        serviceDevice.setDevice(deviceRepository.findOne(deviceId));
        serviceDevice.setServiceOrder(serviceOrder);

        serviceDeviceRepository.save(serviceDevice);

        return "redirect:/order/" + serviceOrder.getServiceOrder();
    }

    @RequestMapping(value = "/{id}/add-action", method = RequestMethod.GET)
    public String addServiceAction(@PathVariable Integer id, Principal principal) {
        ServiceOrder serviceOrder = serviceOrderRepository.findOne(id);
        UserAccount user = userAccountRepository.findByUsername(principal.getName());

        ServiceAction serviceAction = new ServiceAction();
        serviceAction.setCreatedBy(user.getSubject().getPerson());
        serviceAction.setServiceOrder(serviceOrder);
        serviceAction.setPrice(BigInteger.valueOf(0));
        serviceAction.setServiceAmount(BigInteger.valueOf(0));

        serviceActionRepository.save(serviceAction);

        return "redirect:/order/" + serviceOrder.getServiceOrder();
    }

    @RequestMapping(value = "/{id}/add-part", method = RequestMethod.GET)
    public String addServicePart(@PathVariable Integer id, Principal principal) {
        ServiceOrder serviceOrder = serviceOrderRepository.findOne(id);
        UserAccount user = userAccountRepository.findByUsername(principal.getName());

        ServicePart servicePart = new ServicePart();
        servicePart.setCreatedBy(user.getSubject().getPerson());
        servicePart.setServiceOrder(serviceOrder);
        servicePart.setPartCount(0);
        servicePart.setPartPrice(BigInteger.valueOf(0));

        servicePartRepository.save(servicePart);

        return "redirect:/order/" + serviceOrder.getServiceOrder();
    }

}