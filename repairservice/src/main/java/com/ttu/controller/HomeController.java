package com.ttu.controller;

/**
 * @author Siim-Martin Kaasik
 * IDU0200
 */

import com.ttu.configuration.WebSecurityConfiguration;
import com.ttu.model.subject.UserAccount;
import com.ttu.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@EnableAutoConfiguration
@RequestMapping("/")
public class HomeController {

    @Autowired
    UserAccountRepository userAccountRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model, Principal principal) {
        UserAccount user = userAccountRepository.findByUsername(principal.getName());

        if (user.getSubjectType() == WebSecurityConfiguration.SUBJECT_TYPE_EMPLOYEE) {
            return "redirect:/request/all";
        }

        return "redirect:/customer/orders";

    }

}