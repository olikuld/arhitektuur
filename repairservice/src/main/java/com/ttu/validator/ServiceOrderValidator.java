package com.ttu.validator;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.repairService.ServiceAction;
import com.ttu.model.repairService.ServiceDevice;
import com.ttu.model.repairService.ServiceOrder;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

public class ServiceOrderValidator implements Validator {

    private static final int SERVICE_ACTION_STATUS_FINISHED = 2;

    private static final int SERVICE_ORDER_STATUS_FINISHED = 2;
    private static final int SERVICE_ORDER_STATUS_APPRAISED = 3;

    private static final int SERVICE_DEVICE_STATUS_IN_PROGRESS = 1;
    private static final int SERVICE_DEVICE_STATUS_FINISHED = 2;
    private static final int SERVICE_DEVICE_STATUS_RETURNED = 3;

    @Override
    public boolean supports(Class<?> aClass) {
        return ServiceOrder.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ServiceOrder serviceOrder = (ServiceOrder) o;

        if (serviceOrder.getSoStatusType().getSoStatusType() == SERVICE_ORDER_STATUS_FINISHED || serviceOrder.getSoStatusType().getSoStatusType() == SERVICE_ORDER_STATUS_APPRAISED) {
            List<ServiceAction> serviceActions = serviceOrder.getServiceActions();
            for (ServiceAction serviceAction : serviceActions) {
                if (serviceAction.getServiceActionStatusType() == null || serviceAction.getServiceActionStatusType().getServiceActionStatusType() == SERVICE_DEVICE_STATUS_IN_PROGRESS) {
                    errors.rejectValue("soStatusType", "", "Kõik tööd peavad olema valmis!");
                    break;
                }
            }
        }

        if (serviceOrder.getSoStatusType().getSoStatusType() == SERVICE_ORDER_STATUS_APPRAISED) {
            List<ServiceDevice> serviceDevices = serviceOrder.getServiceDevices();
            for (ServiceDevice serviceDevice : serviceDevices) {
                if (serviceDevice.getServiceDeviceStatusType() == null || serviceDevice.getServiceDeviceStatusType().getServiceDeviceStatusType() == SERVICE_DEVICE_STATUS_IN_PROGRESS) {
                    errors.rejectValue("soStatusType", "", "Kõik tööd seadmetega peavad olema lõpetatud!");
                    break;
                }
            }
        }
    }
}
