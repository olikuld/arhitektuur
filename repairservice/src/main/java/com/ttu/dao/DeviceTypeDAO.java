package com.ttu.dao;

/**
 * @author Oliver Kuldmäe
 * IDU0200
 */

import com.ttu.model.repairService.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

@Component
public class DeviceTypeDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DeviceTypeRowMapper rowMapper;

    public void insert(DeviceType deviceType) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("device_type")
                .usingGeneratedKeyColumns("device_type");

        HashMap<String, Object> map = new HashMap<>();

        map.put("super_type", deviceType.getSuperType());
        map.put("level", deviceType.getLevel());
        map.put("type_name", deviceType.getTypeName());

        Number number = simpleJdbcInsert.executeAndReturnKey(map);

        deviceType.setDeviceType(number.intValue());
    }

    public void delete(Integer id) {
        jdbcTemplate.update("DELETE FROM device_type WHERE device_type = ?", id);
    }

    public void delete(DeviceType deviceType) {
        delete(deviceType.getDeviceType());
    }

    public DeviceType findOne(Integer id) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM device_type WHERE device_type = ?", rowMapper, id);
        } catch(DataAccessException e) {
            return null;
        }
    }

    public List<DeviceType> findAll() {
        try {
            return jdbcTemplate.query("SELECT * FROM device_type", rowMapper);
        } catch(DataAccessException e) {
            return null;
        }
    }
}

@Component
class DeviceTypeRowMapper implements RowMapper<DeviceType> {

    @Autowired
    DeviceTypeDAO deviceTypeDAO;

    @Override
    public DeviceType mapRow(ResultSet resultSet, int i) throws SQLException {
        DeviceType deviceType = new DeviceType(resultSet.getInt("level"), resultSet.getString("type_name"));

        deviceType.setDeviceType(resultSet.getInt("device_type"));
        BigDecimal superId = (BigDecimal) resultSet.getObject("super_type");

        if (superId != null && superId.intValue() > 0) {
            deviceType.setSuperType(deviceTypeDAO.findOne(superId.intValue()));
        }

        return deviceType;
    }

}