$(function() {

    $('#request-table').find('tbody tr').click(function () {
        var id = $(this).closest('tr').find('.id').html();
        window.location.replace("/request/" + id);
    });

    $('#order-table').find('tbody tr').click(function () {
        var id = $(this).closest('tr').find('.id').html();
        window.location.replace("/order/" + id);
    });

     $('#device-table').find('tbody tr').click(function () {
        var id = $(this).closest('tr').find('.id').html();
        window.location.replace("/device/" + id);
    });


    $('.save-action').click(function () {
        console.log('Saved action!');
    });


    $('.save-part').click(function () {
        console.log('Saved part!');
    });
    
});