<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <title>Jalanõu</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/style/styles.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <ul class="nav nav-pills">
        <li role="presentation"><a href="s">Servlet</a></li>
        <li role="presentation"><a href="log">Log</a></li>
    </ul>

    <c:if test="${fn:length(errors) gt 0}">
        <c:forEach items="${errors}" var="error">
            <div class="alert alert-danger" role="alert"><strong>Viga!</strong> ${error}</div>
        </c:forEach>
    </c:if>
    <form action="s?action=save" method="post">
        <div class="form-group">
            <label for="id">ID: </label>
            <span id="id">${shoe.id}</span>
            <input type="hidden" name="id" value="${shoe.id}" required>
        </div>

        <div class="form-group">
            <label for="name">Nimi: </label>
            <input class="form-control" value="${shoe.name}" maxlength="20" type="text" name="name" id="name" required>
        </div>

        <div class="form-group">
            <label for="price">Hind: </label>
            <input class="form-control" value="${shoe.price}" max="99999" type="number" name="price" id="price" min="0" required>
        </div>
        <div class="form-group">
            <label for="description">Kirjeldus</label>
            <textarea class="form-control" name="description" id="description" cols="30" rows="10">${shoe.description} </textarea>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
</body>
</html>
