<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Jalanõud</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/style/styles.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>

<div class="container">
    <ul class="nav nav-pills">
        <li role="presentation" class="active"><a href="s">Servlet</a></li>
        <li role="presentation"><a href="log">Log</a></li>
    </ul>

    <h3>Jalanõude nimekiri:</h3>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nimi</th>
            <th>Hind</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${shoes}" var="shoe">
            <tr>
                <td>${shoe.id}</td>
                <td>${shoe.name}</td>
                <td>${shoe.price}</td>
                <td><a href="shoeservice?id=${shoe.id}">Kirjeldus</a></td>
                <td><a href="s?id=${shoe.id}">Muuda</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
