package com.kuldmae.servlet.db;

import com.kuldmae.servlet.model.Shoe;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShoeDAOTest {

    private ShoeDAO dao;

    @Before
    public void setUp() throws Exception {
        dao = new ShoeDAO();
    }

    @After
    public void tearDown() throws Exception {
        dao.closeConnections();
    }

    @org.junit.Test
    public void testFindById() throws Exception {
        String name = "Some Test Shoe";
        int price = 300;
        String description = "Description";

        int id = dao.insert(name, price, description);
        Shoe shoe = dao.findById(id);

        assertTrue(shoe.getId().equals(id) &&
                shoe.getName().equals(name) &&
                shoe.getPrice().equals(price) &&
                shoe.getDescription().equals(description)
        );
    }

    @org.junit.Test
    public void testFindAll() throws Exception {
        String name = "Some Other Test Shoe";
        int price = 300;
        String description = "Description";

        int id = dao.insert(name, price, description);
        List<Shoe> shoes = dao.findAll();

        assertTrue(shoes.parallelStream().anyMatch(
                shoe -> shoe.getId().equals(id) &&
                        shoe.getName().equals(name) &&
                        shoe.getPrice().equals(price)

        ));
    }

    @org.junit.Test
    public void testUpdate() throws Exception {
        String name = "Update Test Shoe";
        int price = 300;
        String description = "Description";

        int id = dao.insert(name, price, description);
        Shoe shoe = dao.findById(id);

        assertTrue(shoe.getId().equals(id) &&
                shoe.getName().equals(name) &&
                shoe.getPrice().equals(price) &&
                shoe.getDescription().equals(description)
        );

        name = "Updated Test Shoe";
        price = 1000;

        shoe.setName(name);
        shoe.setPrice(price);
        dao.update(shoe);
        shoe = dao.findById(id);

        assertTrue(shoe.getId().equals(id) &&
                shoe.getName().equals(name) &&
                shoe.getPrice().equals(price) &&
                shoe.getDescription().equals(description)
        );
    }

    @Test
    public void testInsert() throws Exception {
        int id = dao.insert("Adidas", 100, "Description");
        assertTrue(id > 0);
    }
}