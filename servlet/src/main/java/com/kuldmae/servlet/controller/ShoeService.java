package com.kuldmae.servlet.controller;

import com.kuldmae.servlet.db.ShoeDAO;
import com.kuldmae.servlet.log.ShoeLogger;
import com.kuldmae.servlet.model.Shoe;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import com.google.gson.Gson;
import com.kuldmae.servlet.validator.ShoeValidator;

public class ShoeService extends HttpServlet {

    private ShoeLogger logger;

    @Override
    public void init() throws ServletException {
        super.init();
        this.logger = new ShoeLogger();
        logger.log("ShoeService.init(): mind loodi");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        // res.setContentType("application/json");

        String paramId = request.getParameter("id");
        logger.log("ShoeService.getPlaneById(): " + paramId);

        if (ShoeValidator.hasValidIdParameter(paramId)) {
            PrintWriter out = response.getWriter();
            Integer id = Integer.parseInt(paramId);
            ShoeDAO dao = new ShoeDAO();
            Shoe shoe = null;

            try {
                shoe = dao.findById(id);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (shoe != null) {
                Gson gson = new Gson();
                out.print(gson.toJson(shoe));
            }
        }
    }

}
