package com.kuldmae.servlet.controller;

import com.kuldmae.servlet.db.ShoeDAO;
import com.kuldmae.servlet.log.ShoeLogger;
import com.kuldmae.servlet.model.Shoe;
import com.kuldmae.servlet.model.ShoeForm;
import com.kuldmae.servlet.validator.ShoeValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class ShoeServlet extends HttpServlet {

    private ShoeLogger logger;

    @Override
    public void init() throws ServletException {
        super.init();
        this.logger = new ShoeLogger();
        logger.log("ShoeServlet.init(): mind loodi");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsp = "/shoes.jsp";
        String paramId = request.getParameter("id");
        ShoeDAO dao = new ShoeDAO();

        if (paramId != null) {
            logger.log("ShoeServlet.findById(): " + paramId);

            if (ShoeValidator.hasValidIdParameter(paramId)) {
                Integer id = Integer.parseInt(paramId);
                Shoe shoe = null;

                try {
                    shoe = dao.findById(id);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (shoe != null) {
                    ShoeForm form = new ShoeForm(
                            shoe.getId().toString(),
                            shoe.getName(),
                            shoe.getPrice().toString(),
                            shoe.getDescription());

                    request.setAttribute("shoe", form);
                    System.out.println("Displaying shoe with ID " + id);
                    jsp = "/shoe.jsp";
                } else {
                    System.out.println("Could not find shoe! ID: " + paramId);
                    jsp = "/error.jsp";
                }
            } else {
                System.out.println("Failed to validate ID parameter! ID: " + paramId);
                jsp = "/error.jsp";
            }
        }
        else {
            System.out.println("Displaying all shoes!");
            logger.log("ShoeServlet.findAll()");
            List<Shoe> shoes = null;

            try {
                shoes = dao.findAll();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            request.setAttribute("shoes", shoes);
        }

        try {
            dao.closeConnections();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(jsp);
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        ShoeForm form = new ShoeForm(request.getParameter("id"),
                request.getParameter("name"),
                request.getParameter("price"),
                request.getParameter("description"));

        if (action != null && action.equals("save")) {
            ShoeValidator validator = new ShoeValidator(form);

            List<String> errors = validator.validate();
            request.setAttribute("errors", errors);

            if (errors.size() > 0) {
                System.out.println("Form validation failed!");
            }
            else {
                ShoeDAO dao = new ShoeDAO();
                Shoe shoe = new Shoe(
                    Integer.parseInt(form.getId()),
                    form.getName(),
                    Integer.parseInt(form.getPrice()),
                    form.getDescription());

                System.out.println("Updating object with id: " + form.getId());
                try {
                    dao.update(shoe);
                    dao.closeConnections();
                } catch (SQLException e) {
                    System.out.println("Failed to update object with id: " + form.getId());
                    errors.add("Jalanõu salvestamisel tekkis viga!");
                    e.printStackTrace();
                }
            }
        }

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/shoe.jsp");
        request.setAttribute("shoe", form);
        dispatcher.forward(request, response);
    }

}
