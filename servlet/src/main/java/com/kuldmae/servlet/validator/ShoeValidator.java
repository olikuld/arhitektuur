package com.kuldmae.servlet.validator;

import com.kuldmae.servlet.model.ShoeForm;

import java.util.ArrayList;
import java.util.List;

public class ShoeValidator {

    private ShoeForm form;

    public ShoeValidator(ShoeForm form) {
        this.form = form;
    }

    public List<String> validate() {
        List<String> errors = new ArrayList<String>();

        if (form.getName().equals("")) {
            System.out.println("Form validation failure - invalid name: " + form.getName());
            errors.add("Nimi on kohustuslik!");
        } else if(form.getName().length() > 20) {
            System.out.println("Form validation failure - invalid name: " + form.getName());
            errors.add("Nimi on liiga pikk!");
        }

        try {
            Integer id = Integer.parseInt(form.getId());

            if (id < 0 || id >= 1000000) {
                System.out.println("Form validation failure - invalid ID: " + id);
                errors.add("Ebakorrektne ID!");
            }
        } catch (Exception e) {
            errors.add("Ebakorrektne ID!");
        }

        try {
            Integer price = Integer.parseInt(form.getPrice());

            if (price < 0) {
                System.out.println("Form validation failure - invalid price: " + price);
                errors.add("Hind peab olema suurem kui 0!");
            } else if (price >= 100000) {
                System.out.println("Form validation failure - invalid price: " + price);
                errors.add("Hind peab olema väiksem kui 100000!");
            }
        } catch (Exception e) {
            errors.add("Ebakorrektne hind!");
        }

        return errors;
    }

    public static boolean hasValidIdParameter(String id) {
        if (id == null) {
            return false;
        }

        try {
            Integer shoeId = Integer.parseInt(id);

            if (shoeId > 0) {
                return true;
            } else {
                System.out.println("Invalid shoe ID");
            }
        } catch (Exception e) {
            System.out.println("Failed to parse integer!");
            return false;
        }

        return false;
    }
}
