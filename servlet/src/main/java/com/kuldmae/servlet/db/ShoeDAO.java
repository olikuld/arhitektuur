package com.kuldmae.servlet.db;

import com.kuldmae.servlet.model.Shoe;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ShoeDAO {

    private Connection connection;
    private ResultSet result;
    private PreparedStatement statement;

    public ShoeDAO() {
        try {
            Class.forName("org.postgresql.Driver");
            this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/servlet");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Shoe findById(int id) throws SQLException {
        String sql = "SELECT * FROM SHOE WHERE ID = ?";

        statement = connection.prepareStatement(sql);
        statement.setInt(1, id);
        statement.setMaxRows(1);
        result = statement.executeQuery();
        result.next();

        return new Shoe(
                result.getInt(1),
                result.getString(2),
                result.getInt(3),
                result.getString(4)
        );
    }

    public List<Shoe> findAll() throws SQLException {
        List<Shoe> shoes = new ArrayList<>();
        String sql = "SELECT * FROM SHOE ORDER BY ID";

        statement = connection.prepareStatement(sql);
        result = statement.executeQuery();

        while (result.next()) {
            shoes.add(new Shoe(
                    result.getInt(1),
                    result.getString(2),
                    result.getInt(3),
                    result.getString(4))
            );
        }

        return shoes;
    }

    public void update(Shoe shoe) throws SQLException {
        String sql =
                "UPDATE SHOE SET " +
                        "name = ?, " +
                        "price = ?, " +
                        "description = ? " +
                        "WHERE ID = ?";

        statement = connection.prepareStatement(sql);
        statement.setString(1, shoe.getName());
        statement.setInt(2, shoe.getPrice());
        statement.setString(3, shoe.getDescription());
        statement.setInt(4, shoe.getId());
        statement.executeUpdate();
    }

    public int insert(String name, int price, String description) throws SQLException {
        String sql =
                "INSERT INTO SHOE (NAME, PRICE, DESCRIPTION) " +
                        "VALUES (?, ?, ?)";

        statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, name);
        statement.setInt(2, price);
        statement.setString(3, description);

        int affectedRows = statement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Creating Shoe failed, no rows affected.");
        }

        try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            }
            else {
                throw new SQLException("Creating user failed, no ID obtained.");
            }
        }

    }

    public void closeConnections() throws SQLException {
        if (statement != null) {
            statement.close();
        }
        if (connection != null) {
            connection.close();
        }
        if (result != null) {
            result.close();
        }
    }
}
